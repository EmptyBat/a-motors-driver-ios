//
//  HistoryAdditionalFirstTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 27.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class HistoryAdditionalFirstTableViewCell: UITableViewCell {
    @IBOutlet weak var districtFrom_lb: UILabel!
    
    @IBOutlet weak var distance_lb: UILabel!
    @IBOutlet weak var wait_time: UILabel!
    @IBOutlet weak var timeEnd_lb: UILabel!
    @IBOutlet weak var timeStart_lb: UILabel!
    @IBOutlet weak var year_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
