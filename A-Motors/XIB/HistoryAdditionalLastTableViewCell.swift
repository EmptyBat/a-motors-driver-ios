//
//  HistoryAdditionalLastTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 30.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class HistoryAdditionalLastTableViewCell: UITableViewCell {
    @IBOutlet weak var title_lb: UILabel!
    @IBOutlet weak var totalStats_lb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
