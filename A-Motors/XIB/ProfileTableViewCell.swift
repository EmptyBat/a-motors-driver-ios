//
//  ProfileTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 28.06.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var username_lb: UILabel!
    @IBOutlet weak var avatar_img: UIImageView!
    @IBOutlet weak var phone_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        avatar_img.layer.borderWidth = 6
        avatar_img.layer.masksToBounds = false
        avatar_img.layer.borderColor = BaseMessage.textcolor.cgColor
        avatar_img.layer.cornerRadius = avatar_img.frame.height/2
        avatar_img.clipsToBounds = true
        let mScreenSize = UIScreen.main.bounds
        let mSeparatorHeight = CGFloat(0.4) // Change height of speatator as you want
        let mAddSeparator = UIView.init(frame: CGRect(x: 0, y: self.frame.size.height - mSeparatorHeight, width: mScreenSize.width*0.85, height: mSeparatorHeight))
        mAddSeparator.backgroundColor = UIColor.white // Change backgroundColor of separator
        self.addSubview(mAddSeparator)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
