//
//  OrderPullReserveTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 11/18/18.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class OrderPullReserveTableViewCell: UITableViewCell {
    @IBOutlet weak var name_lb: UILabel!
    @IBOutlet weak var district_lb: UILabel!
    @IBOutlet weak var time_lb: UILabel!
    @IBOutlet weak var comment_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
