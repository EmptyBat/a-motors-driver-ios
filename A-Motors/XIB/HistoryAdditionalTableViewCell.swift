//
//  HistoryAdditionalTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 27.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class HistoryAdditionalTableViewCell: UITableViewCell {
    @IBOutlet weak var title_lb: UILabel!
    @IBOutlet weak var distance_lb: UILabel!
    @IBOutlet weak var time_lb: UILabel!
    @IBOutlet weak var icon_img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
