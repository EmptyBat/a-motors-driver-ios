//
//  OrderPullTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 01.08.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class OrderPullTableViewCell: UITableViewCell {
    @IBOutlet weak var name_lb: UILabel!
    @IBOutlet weak var district_lb: UILabel!
    @IBOutlet weak var time_lb: UILabel!
    @IBOutlet weak var comment_lb: UILabel!
//  orderPull_cell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
