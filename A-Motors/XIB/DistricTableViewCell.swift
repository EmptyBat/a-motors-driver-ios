//
//  Distric_TablewViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 28.06.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class DistricTableViewCell: UITableViewCell {
    var phone_number: String?
    @IBOutlet weak var icon_img: UIImageView!
    @IBOutlet weak var district: UILabel!
    @IBOutlet weak var background_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupData(_ order: OrderInfo) {
        district.text = order.data_from
    }
    
}
