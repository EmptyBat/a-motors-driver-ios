//
//  BeginDistrictTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 18.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class BeginDistrictTableViewCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var district_lb: UILabel!
    @IBOutlet weak var drop_down_btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
