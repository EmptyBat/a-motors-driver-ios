//
//  TaxiInfoTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 27.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class TaxiInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var brand_lb: UILabel!
    @IBOutlet weak var registrationNumber_lb: UILabel!
    
    @IBOutlet weak var call_btn: UIButton!
    @IBOutlet weak var color_view: UIView!
    @IBOutlet weak var color_lb: UILabel!
    @IBOutlet weak var name_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func call_btn(_ sender: Any) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
