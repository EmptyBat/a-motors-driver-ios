//
//  CasualTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 06.08.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class CasualTableViewCell: UITableViewCell {
    @IBOutlet weak var district_lb: UILabel!
    @IBOutlet weak var generalWait_lb: UILabel!
    
    @IBOutlet weak var intermediaWait_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
