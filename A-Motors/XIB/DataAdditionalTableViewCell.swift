//
//  DataAdditionalTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 27.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class DataAdditionalTableViewCell: UITableViewCell {

    @IBOutlet weak var title_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
