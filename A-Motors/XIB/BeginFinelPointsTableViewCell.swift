//
//  BeginFinelPointsTableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 17.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class BeginFinelPointsTableViewCell: UITableViewCell {
    @IBOutlet weak var district: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
