//
//  Info_TableViewCell.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 28.06.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    @IBOutlet weak var info_lb: UILabel!
    @IBOutlet weak var pre_info_lb: UILabel!
    @IBOutlet weak var call_btn: UIButton!
    @IBOutlet weak var order_at_lb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func call_btn(_ sender: UIButton) {
    }
    func setupData(_ passanger: Passanger) {
        info_lb.text = passanger.name  + " " + passanger.surname + "" + passanger.patronymic
    }
    
}
