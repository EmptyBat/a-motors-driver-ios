//
//  MenuTableViewController.swift
//  Example4Swift
//
//  Created by Patrick BODET on 09/08/2016.
//  Copyright © 2016 iDevelopper. All rights reserved.
//

import UIKit
import PBRevealViewController
import SDWebImage
import SVProgressHUD
class MenuTableViewController: UITableViewController {
    var listData = ["История поездок","Заработок", "Фотоконтроль", "Связь с диспетчером", "Выйти"]
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count+1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0){
            return 157
        }
        return 60
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "profile_cell", for: indexPath) as! ProfileTableViewCell
            cell.username_lb.text = UserDefaults.standard.string(forKey: "name")
            cell.phone_lb.text = UserDefaults.standard.string(forKey: "phone")
            cell.avatar_img.sd_setImage(with: URL(string:UserDefaults.standard.string(forKey: "photo")!), placeholderImage: UIImage(named: ""))
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideBar_cell", for: indexPath) as! SideBarDataTableViewController
            cell.title_lb.text = listData[indexPath.row-1]
            if UserDefaults.standard.bool(forKey: "additional_bool") == false {
                if (indexPath.row == listData.count-1){
                    cell.call_btn.isHidden = false
                }
            }
            else {
                if (indexPath.row == listData.count-2){
                    cell.call_btn.isHidden = false
                }
            }
            return cell
            
        }
    }
    func configUI() {
        let notificationName = Notification.Name("force_finish_add")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(forceFinish),
            name: notificationName,
            object: nil)
        if UserDefaults.standard.bool(forKey: "additional_bool") == true {
            listData = ["История поездок","Заработок", "Фотоконтроль", "Связь с диспетчером","Завершить заказ", "Выйти"]
        }
        tableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "profile_cell")
        tableView.register(UINib(nibName: "SideBarDataTableViewController", bundle: nil), forCellReuseIdentifier: "SideBar_cell")
    }
     @objc func forceFinish(notification: NSNotification){
        if UserDefaults.standard.bool(forKey: "additional_bool") == true {
            listData = ["История поездок","Заработок", "Фотоконтроль", "Связь с диспетчером","Завершить заказ", "Выйти"]
        }
        else {
            listData = ["История поездок","Заработок", "Фотоконтроль", "Связь с диспетчером", "Выйти"]
        }
        tableView.reloadData()
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if (indexPath.row == 5) {
            if UserDefaults.standard.bool(forKey: "additional_bool") == false {
            if UserDefaults.standard.bool(forKey: "arrived") == false {
                if UserDefaults.standard.bool(forKey: "driverIsBusy") == false {
                    AuthManager.shared.logout()
                }
            }
            }
            else {
                let data_keyFrom = UserDefaults.standard.string(forKey: "data_keyFrom")!
                if (data_keyFrom == "data_additional_1" ||  data_keyFrom == "data_additional_2"  ||  data_keyFrom == "data_additional_3"
                ||  data_keyFrom == "data_additional_4"
                    ||  data_keyFrom == "data_additional_5"){
                force_finish()
            }
                else {
                SVProgressHUD.showSuccess(withStatus: BaseMessage.force_finish)
                }
            }
        }
        if (indexPath.row == 4){
            guard let number = URL(string: "tel://" + "+77779998488") else { return }
            UIApplication.shared.open(number)
        }
        if (indexPath.row == 1) {
            self.revealViewController()?.revealLeftView()
            performSegue(withIdentifier: "history", sender: self)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        // MARK: - Table view delegate
    }
    func force_finish(){
        let data_keyFrom = UserDefaults.standard.string(forKey: "data_keyFrom")!
        let driver_id = UserDefaults.standard.integer(forKey: "driver_id")
        let order_id  = UserDefaults.standard.string(forKey: "order_id")!
        NetworkManager.makeRequest(target: .force_finish(order_id: order_id, driver_id: driver_id, data_key: data_keyFrom)) { (json) in
            print("asffsa",json)
            self.revealViewController()?.revealLeftView()
            let finish = Notification.Name("new_order")
             NotificationCenter.default.post(name: finish, object: nil)
            self.performSegue(withIdentifier: "complete", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let driver_id = UserDefaults.standard.integer(forKey: "driver_id")
        let order_id  = UserDefaults.standard.integer(forKey: "order_id")
        if segue.identifier == "complete" {
            let destination: TravelDetailsController = segue.destination as! TravelDetailsController
            destination.orderid = order_id
            destination.driver_id = driver_id
            destination.force_finish = true
        }
    }
}
