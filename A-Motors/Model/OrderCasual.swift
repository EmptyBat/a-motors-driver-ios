//
//  OrderCasual.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 06.08.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
struct OrderCasual{
    
    let wait: Int
    let title: String
    
    init(json: JSON) {
        self.wait = json["stats"]["wait"].intValue
        self.title = json["data"]["name"].stringValue
    }
    
}

