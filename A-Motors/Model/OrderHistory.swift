//
//  OrderHistory.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 27.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
struct OrderHistory{
    
    let started_at: String
    let finished_at: String
    let title: String
    let longitude: String
    let latitude: String
    let name: String
    let data_fromLongitude: String
    let data_fromLatitude: String
    let data_fromName: String
    let data_toLongitude: String
    let data_toLatitude: String
    let data_toName: String
    let company: String
    let history_id: Int
        
    init(json: JSON) {
        self.started_at = json["information"]["exists"]["data_to"]["time"]["started"].stringValue
        self.finished_at = json["finished_at"].stringValue
        self.title = json["company"]["title"].stringValue
        self.longitude = json["longitude"].stringValue
        self.latitude = json["latitude"].stringValue
        self.name = json["name"].stringValue
        self.company = json["company"].stringValue
        self.data_fromLongitude = json["data_from"]["longitude"].stringValue
        self.data_fromLatitude = json["data_from"]["latitude"].stringValue
        self.data_fromName = json["data_from"]["name"].stringValue
        self.data_toLongitude = json["data_to"]["longitude"].stringValue
        self.data_toLatitude = json["data_to"]["latitude"].stringValue
        self.data_toName = json["data_to"]["name"].stringValue
        self.history_id = json["id"].intValue

    }
}
