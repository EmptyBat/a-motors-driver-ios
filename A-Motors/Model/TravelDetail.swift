//
//  TravelDetail.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 27.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
struct TravelDetail{
    
    let started_at: String
    let finished_at: String
    let title: String
    let longitude: String
    let latitude: String
    let name: String
    let data_fromLongitude: String
    let data_fromLatitude: String
    let data_fromName: String
    let data_toLongitude: String
    let data_toLatitude: String
    let data_toName: String
    let driverName: String
    let driverSurname: String
    let driverPatronymic: String
    let registration_number: String
    let brand: String
    let color: String
    let colorHex: String
    let driverPhone: String

    init(json: JSON) {
        self.started_at = json["started_at"].stringValue
        self.finished_at = json["finished_at"].stringValue
        self.title = json["company"]["title"].stringValue
        self.longitude = json["longitude"].stringValue
        self.latitude = json["latitude"].stringValue
        self.name = json["name"].stringValue
        self.data_fromLongitude = json["data_from"]["longitude"].stringValue
        self.data_fromLatitude = json["data_from"]["latitude"].stringValue
        self.data_fromName = json["data_from"]["name"].stringValue
        self.data_toLongitude = json["data_to"]["longitude"].stringValue
        self.data_toLatitude = json["data_to"]["latitude"].stringValue
        self.data_toName = json["data_to"]["name"].stringValue
        self.driverName = json["driver"]["name"].stringValue
        self.driverPhone = json["driver"]["phone"].stringValue
        self.driverSurname = json["driver"]["surname"].stringValue
        self.driverPatronymic = json["driver"]["patronymic"].stringValue
        self.registration_number = json["driver"]["car"]["registration_number"].stringValue
        self.brand = json["driver"]["car"]["brand"]["name"].stringValue
        self.color = json["driver"]["car"]["color"]["name"].stringValue
        self.colorHex = json["driver"]["car"]["color"]["hex_code"].stringValue
        
    }
}
