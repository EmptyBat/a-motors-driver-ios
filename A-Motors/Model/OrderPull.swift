//
//  OrderPull.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 01.08.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
struct OrderPull{
    
    let order_at: String
    let title: String
    let data_fromLongitude: String
    let data_fromLatitude: String
    let data_fromName: String
    let passanger_name: String
    let order_id: String
    let comment: String
    let preliminary : Bool

    init(json: JSON) {
        self.order_id = json["id"].stringValue
        self.order_at = json["order_at"].stringValue
        self.title = json["company"]["title"].stringValue
        self.data_fromLongitude = json["data_from"]["longitude"].stringValue
        self.data_fromLatitude = json["data_from"]["latitude"].stringValue
        self.data_fromName = json["data_from"]["name"].stringValue
        self.passanger_name = json["passanger_name"].stringValue
        self.comment = json["comment"].stringValue
        self.preliminary = json["preliminary"].boolValue
    }

}
