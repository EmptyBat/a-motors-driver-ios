//
//  PassengerInfo.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 04.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Passanger{
    
    let id: Int
    let name: String
    let surname: String
    let patronymic: String
    let email: String
    let phone: String
    init(json: JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.surname = json["surname"].stringValue
        self.patronymic = json["patronymic"].stringValue
        self.email = json["email"].stringValue
        self.phone =  json["phone"].stringValue
    
    }
}

