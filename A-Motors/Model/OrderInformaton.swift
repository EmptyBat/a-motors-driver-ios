//
//  OrderInformaton.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 06.08.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
struct OrderInformation{
    
    let time: Int
    let distance: Int
    
    init(json: JSON) {
        self.time = json["time"].intValue
        self.distance = json["distance"].intValue
    }
}

