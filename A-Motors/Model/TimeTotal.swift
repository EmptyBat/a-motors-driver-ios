//
//  TimeTotal.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 07.08.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
struct TimeTotal{
    
    let time_total: Int
    let time_drive: Int
    let time_wait: Int
    let distance: Int
    
    init(json: JSON) {
        self.time_total = json["time_total"].intValue
        self.time_drive = json["time_drive"].intValue
        self.time_wait = json["time_wait"].intValue
        self.distance = json["distance"].intValue
    }
}
