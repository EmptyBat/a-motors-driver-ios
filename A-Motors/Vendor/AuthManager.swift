//
//  AuthManager.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 02.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SDWebImage
import Moya
struct AuthManager {
    
    static let shared: AuthManager = {
        let instance = AuthManager()
        return instance
    }()
    func logout(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LOGOUT")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc
        UserDefaults.standard.removeObject(forKey: "status_sw")
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "driverIsBusy")
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        UserDefaults.standard.synchronize()
    }
    
    func login(_ user_id: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ordernavig")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc
        UserDefaults.standard.setValue(user_id, forKey: "user_id")
        UserDefaults.standard.synchronize()
    }
    func check_login() {
        NetworkManager.makeRequest(target: .getConfig()){ [self]
            (json) in
            guard self != nil else { return }
            UserDefaults.standard.setValue(json["frequency_without_order"].intValue, forKey: "frequency_without_order")
            UserDefaults.standard.setValue(json["frequency_in_order"].intValue, forKey: "frequency_in_order")
            UserDefaults.standard.synchronize()
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
         if let userid = UserDefaults.standard.string(forKey: "user_id") {
            if UserDefaults.standard.bool(forKey: "driverIsBusy") == true {
            let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = vc
           }
        else {
            let vc = storyboard.instantiateViewController(withIdentifier: "ordernavig")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = vc
        }
        } else {
            let vc = storyboard.instantiateViewController(withIdentifier: "LOGOUT")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = vc
        }
    }
}
