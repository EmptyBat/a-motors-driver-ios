//
//  AppDelegate.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 25.06.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import UserNotifications
import Presentr
import Firebase
import SwiftyJSON
import FirebaseMessaging
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let authM = AuthManager()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
          //             registerForPushNotifications()
        do {
            Network.reachability = try Reachability(hostname: "www.google.com")
            do {
                try Network.reachability?.start()
            } catch let error as Network.Error {
                print(error)
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }

        application.registerForRemoteNotifications()
        // For iOS 10 data message (sent via FCM)
        authM.check_login()
        setActionCategories()
        return true
    }
    func setActionCategories(){
        
        let acceptAction = UNNotificationAction(
            identifier: "snooze.action",
            title: "Подтведрить",
            options: [.foreground])
        
        let rejectAction = UNNotificationAction(
            identifier: "snooze.action",
            title: "Отказаться",
            options: [.foreground])
        
        let snoozeCategory = UNNotificationCategory(
            identifier: "ACCEPT.category",
            actions: [acceptAction,rejectAction],
            intentIdentifiers: [],
            options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories(
            [snoozeCategory])
        
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings)
    {
        application.registerForRemoteNotifications()
    }
    let customPresenter: Presentr = {
        
        let width = ModalSize.full
        let height = ModalSize.custom(size: 150)
        let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: 0))
        
        let customType = PresentationType.custom(width: width, height: height, center: center)
        
        let customPresenter = Presentr(presentationType: customType)
        customPresenter.transitionType = .coverVerticalFromTop
        customPresenter.roundCorners = false
        return customPresenter
        
    }()
    func registerPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                UIApplication.shared.registerUserNotificationSettings(settings)
        }
          //      registerForPushNotifications()
                UIApplication.shared.registerForRemoteNotifications()
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        registerDeviceToken(pushToken: token)
        print("Device Token: \(token)")
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    func goToMain(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc
    }
    func registerDeviceToken(pushToken:String) {
        
        if let savedToken = UserDefaults.standard.value(forKey: "UserPushToken") as? String {
            if savedToken == pushToken {
                return
            }
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // Print notification payload data
        print("give me status # 1")
        if let userInfo1 =  userInfo[AnyHashable("gcm.notification.data")] as? String {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = vc
            var json = JSON.parse(userInfo1)
            let order_id = json["id"].stringValue
            let notificationName = Notification.Name("push")
            NotificationCenter.default.post(name: notificationName, object: userInfo1)
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
           UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
          UIApplication.shared.applicationIconBadgeNumber = 0
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
          UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
          UIApplication.shared.applicationIconBadgeNumber = 0
    }


}
/*func registerForPushNotifications() {
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
        (granted, error) in
        print("Permission granted: \(granted)")
        
        guard granted else { return }
        
        // 1
        let viewAction = UNNotificationAction(identifier: "category_news",
                                              title: "Подтвердить",
                                              options: [.foreground])
        
        // 2
        let newsCategory = UNNotificationCategory(identifier: "category_news",
                                                  actions: [viewAction],
                                                  intentIdentifiers: [],
                                                  options: [])
        // 3
        UNUserNotificationCenter.current().setNotificationCategories([newsCategory])
        
        getNotificationSettings()
    }
}*/
func getNotificationSettings() {
    UNUserNotificationCenter.current().getNotificationSettings { (settings) in
        print("Notification settings: \(settings)")
    }
}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
     /*   let userInfo = notification.request.content.userInfo
        print("give me status # 1",userInfo)
        if let userInfo1 =  notification.request.content.userInfo[AnyHashable("gcm.notification.data")] as? String {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = vc
            var json = JSON.parse(userInfo1)
              let order_id = json["id"].stringValue
                   print ("order_idsss",order_id)
                print ("order_idsss",json)
                let notificationName = Notification.Name("push")
                NotificationCenter.default.post(name: notificationName, object: userInfo1)
            
                 print ("dddsdds",userInfo1)
        }
       */
         //print(notification)
        // Change this to your preferred presentation option
      completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let state: UIApplicationState = UIApplication.shared.applicationState
        let action = response.actionIdentifier
        let request = response.notification.request
        let content = request.content
        if action == "snooze.action"{
            let snoozeTrigger = UNTimeIntervalNotificationTrigger(
                timeInterval: 5.0,
                repeats: false)
            let snoozeRequest = UNNotificationRequest(
                identifier: "pizza.snooze",
                content: content,
                trigger: snoozeTrigger)
            center.add(snoozeRequest){
                (error) in
                if error != nil {
                    print("Snooze Request Error: \(error?.localizedDescription)")
                }
            }
        }

        // Print full message.
                    print("give me status # 2",response.notification.request.content.userInfo)
        if let userInfo1 =  response.notification.request.content.userInfo[AnyHashable("gcm.notification.data")] as? String {
            var json = JSON.parse(userInfo1)
            let driver_id = json["driver_id"].intValue
                goToMain()
    
                let new_order = Notification.Name("new_order")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    NotificationCenter.default.post(name: new_order, object: userInfo1)
            
            }
        }
        
        // 2
            //(window?.rootViewController as? UITabBarController)?.selectedIndex = 1
            
            // 3
           completionHandler()
    }
    }
// [END ios_10_messаage_handling]


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "UserPushToken")
        setDeviceToken(fcmToken: fcmToken)
        resetBadgeCount(fcmToken: fcmToken)
        UIApplication.shared.applicationIconBadgeNumber = 0
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("give me status # 3")
        if let userInfo1 =  userInfo[AnyHashable("gcm.notification.data")] as? String {
            var json = JSON.parse(userInfo1)
            let driver_id = json["driver_id"].intValue
            if driver_id == Int(UserDefaults.standard.string(forKey: "user_id")!) {
            let notificationName = Notification.Name("push")
            goToMain()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                NotificationCenter.default.post(name: notificationName, object: userInfo1)
            }
            }
            else {
               let new_order = Notification.Name("new_order")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    NotificationCenter.default.post(name: new_order, object: userInfo1)
                }
            }
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }

    func setDeviceToken(fcmToken:String) {
        if ((UserDefaults.standard.string(forKey: "user_id")) != nil) {
            NetworkManager.makeRequest(target: .setDeviceToken(params:["token": fcmToken],entity_id:UserDefaults.standard.string(forKey: "user_id")!,device_token:fcmToken)) { (json) in
            UserDefaults.standard.set(fcmToken, forKey: "UserPushToken")
        }
    }
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func resetBadgeCount(fcmToken:String){
        NetworkManager.makeRequest(target: .reset_badge_count(token :fcmToken)) { (json) in
        UIApplication.shared.applicationIconBadgeNumber = 0
        }
        }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}



