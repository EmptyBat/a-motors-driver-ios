//
//  BaseMessage.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 04.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

struct BaseMessage {
    static var ImageUrl = "http://crm.amotors.kz"
    static var baseUrl = "http://crm.amotors.kz/api/v1"
    // static let appColor = UIColor(red:0.49, green:0.19, blue:0.89, alpha:1.0)
    static let appColor = UIColor.white
    static let textcolor = UIColor.init(red: 0/255, green: 152/255, blue: 244/255, alpha: 1)
    static let passwordError = "Неправильный логин или пароль"
    static let parsingError = "Ошибка в обработке данных"
    static let fillError = "Заполните всю необходимую информацию"
    static let commentAdded = "Комментарий отправлен на модерацию"
    static let wrongcode = "Неверный код"
    static let locationdisabled = "Включите геолокацию"
    static let orderSucces = "Заказ завершен"
    static let orderTime = "Время заказа еще не подошло"
    static let orderRemoved = "Заказ Отменен"
    static let unFortune = "Не успел"
    static let force_finish = "Вы находитесь не на промежуточной точке"
    static let internet_error = "Плохое интернет соединение"
    static let preliminary_order = "Время заказа еще не подошло"
}
