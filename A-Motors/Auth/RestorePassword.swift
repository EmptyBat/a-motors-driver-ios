//
//  RestorePassword.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 04.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import InputMask
import SVProgressHUD
class RestorePassword: UIViewController ,MaskedTextFieldDelegateListener{
    var maskedDelegate: MaskedTextFieldDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubObjects()
        CreateConstraints()
        configureUI()
        
    }
    let label_text: UILabel = {
        var view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Для восстановления пароля введите номер телефона указанный при регистрации, на него будет отпрален пароль"
        view.textAlignment = .center
        view.numberOfLines = 0
        view.font = view.font.withSize(14)
        view.textColor = UIColor.black
        return view
    }()
    func configureUI() {
        maskedDelegate = MaskedTextFieldDelegate(format: "{+7} ([000]) [000] [00] [00]")
        maskedDelegate?.listener = self
        phone_tf.delegate = maskedDelegate
      //  phone_tf.becomeFirstResponder()
    }
    func addSubObjects() {
        view.addSubview(header_img)
        view.addSubview(label_text)
        view.addSubview(phone_tf)
        view.addSubview(forgon_btn)
    }
    let header_img: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "restore_background")!
        view.image = image
        view.contentMode = .scaleToFill
        view.clipsToBounds = true
        return view
    }()
    let phone_tf: SkyFloatingLabelTextField = {
        let view = SkyFloatingLabelTextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = NSAttributedString(string: "+7(_ _ _)_ _ _ _ _ _",
                                                        attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        view.textColor = UIColor.black
        view.lineColor = UIColor.white
        view.textAlignment = .left
        view.selectedLineColor = UIColor.lightGray
        view.selectedTitleColor = UIColor.lightGray
        return view
    }()
    let forgon_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("НАПОМНИТЬ ПАРОЛЬ",for: .normal)
        // view.backgroundColor = UIColor(red: 63/255, green: 135/255, blue: 245/255, alpha: 1.0)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(forgon_btn_clicked), for: .touchUpInside)
        return view
    }()
    func textFieldDidBeginEditing(_ textField: UITextField) {
        phone_tf.attributedPlaceholder = NSAttributedString(string: "Пароль",
                                                                      attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
    }
    func CreateConstraints()  {
        // constraints for header image
        header_img.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        header_img.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        header_img.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        header_img.heightAnchor.constraint(equalToConstant: 290).isActive = true 
        header_img.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant:-self.view.frame.height - 290).isActive = true
        header_img.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        // constraints for label text
        label_text.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        label_text.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        label_text.topAnchor.constraint(equalTo: header_img.bottomAnchor, constant: 80).isActive = true
        label_text.heightAnchor.constraint(equalToConstant: 70).isActive = true
        // constraints for phone textfield
        phone_tf.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        phone_tf.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        phone_tf.topAnchor.constraint(equalTo: label_text.bottomAnchor, constant: 40).isActive = true
        phone_tf.heightAnchor.constraint(equalToConstant: 50).isActive = true
        // constraints for forgot button
        forgon_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        forgon_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        forgon_btn.topAnchor.constraint(equalTo: phone_tf.bottomAnchor, constant: 100).isActive = true
        forgon_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    @objc func forgon_btn_clicked(_ sender: AnyObject?) {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
