//
//  AuthViewModel.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 02.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import Foundation
import SVProgressHUD
import UIKit

class AuthViewModel: NSObject {
    
    // MARK: - Auth API Routes
    
    func signIn(phone: String, password: String) {
        NetworkManager.makeRequest(target: .signIn(params: ["phone": phone, "password": password])) { (json) in
            UserDefaults.standard.set(json["data"]["name"].stringValue + " " + json["data"]["surname"].stringValue, forKey: "name")
            UserDefaults.standard.set(json["data"]["phone"].stringValue, forKey: "phone")
            UserDefaults.standard.set(json["data"]["id"].stringValue, forKey: "user_id")
             UserDefaults.standard.set(json["data"]["registration_number"].stringValue, forKey: "car_id")
            UserDefaults.standard.set(json["data"]["city_id"].stringValue, forKey: "city_id")
            UserDefaults.standard.set(BaseMessage.ImageUrl+json["data"]["photo"].stringValue, forKey: "photo")
            if let userId = json["data"]["id"].int {
               AuthManager.shared.login(userId)
            }
            NetworkManager.makeRequest(target: .setDeviceToken(params:["token": UserDefaults.standard.string(forKey: "user_id")],entity_id:UserDefaults.standard.string(forKey: "user_id")!,device_token:UserDefaults.standard.string(forKey: "UserPushToken")!)) { (json) in
            }
        }
    }
    func getNewPassword(phone:String) {
        NetworkManager.makeRequest(target: .getNewPassword(phone:phone)) { (json) in
            
        }
    }
    func signUp(params: [String: Any]) {
     /*   NetworkManager.makeRequest(target: .signUp(params: params), success: { (json) in
            if let userId = json["data"]["user_id"].int {
                AuthManager.shared.login(userId)
            }
        })*/
    }
    func forgotPassword(email: String, callBack: @escaping () -> Void) {
     /*   NetworkManager.makeRequest(target: .forgotPassword(params: ["email" : email])) { (json) in
            SVProgressHUD.showSuccess(withStatus: "На ваш E-mail оптправлена ссылка с восстановлением пароля. Необходимо проверить почту.")
            callBack()
        }*/
    }
}

