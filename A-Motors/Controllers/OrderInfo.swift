//
//  OrderInfo.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 12.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit

import SwiftyJSON

struct OrderInfo{
    
    let from_title: String
    let passanger_id: Int
    let order_id: Int
    let data_from: String
    let longitude: String
    let latitude: String
    let data_additional: String
    let data_to: String
    let name: String
    let driver_id: Int
    let longitude_to: String
    let latitude_to: String
    let longitude_from : String
    let latitude_from : String
    let longitude_additional : String
    let latitude_additional : String
    
    init(json: JSON) {
        self.from_title = json["from_title"].stringValue
        self.passanger_id = json["passanger_id"].intValue
        self.order_id = json["id"].intValue
        self.data_from = json["data_from"]["name"].stringValue
        self.longitude_from = json["data_from"]["longitude"].stringValue
        self.latitude_from = json["data_from"]["latitude"].stringValue
        self.longitude = json["longitude"].stringValue
        self.latitude = json["latitude"].stringValue
        self.data_additional = json["data_additional"]["name"].stringValue
        self.longitude_additional = json["data_additional"]["longitude"].stringValue
        self.latitude_additional = json["data_additional"]["latitude"].stringValue
        self.data_to = json["data_to"]["name"].stringValue
        self.name = json["name"].stringValue
        self.driver_id = json["driver_id"].intValue
        self.longitude_to = json["data_to"]["longitude"].stringValue
        self.latitude_to = json["data_to"]["latitude"].stringValue
        
    }
}
