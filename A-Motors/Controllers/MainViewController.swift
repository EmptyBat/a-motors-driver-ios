//
//  MainViewController.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 13.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//
import UIKit
import CoreLocation
import InputMask
import SwiftyJSON
import PBRevealViewController
import WebKit
import SVProgressHUD
import PusherSwift
import Presentr

class MainViewController: UIViewController, WKNavigationDelegate,UITableViewDelegate,UITableViewDataSource ,PBRevealViewControllerDelegate,CLLocationManagerDelegate,PusherDelegate{
    
    @IBOutlet weak var menu_btn: UIButton!
    var pusher: Pusher! = nil
    var botton_btn : NSLayoutConstraint?
    @IBOutlet weak var status_sw: UISwitch!
    @IBOutlet weak var switch_lb: UILabel!
    var passanger: [Passanger] = []
    var order: [OrderInfo] = []
    var data_additional: [OrderInfo] = []
    let  userid = Int(UserDefaults.standard.string(forKey: "user_id")!)
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var status_lb: UILabel!
    @IBOutlet weak var tv_height: NSLayoutConstraint!
    @IBOutlet weak var webView: WKWebView!
    static let time_interval: TimeInterval = 10
    var inter_color : [UIColor] = []
    var stepSizes : [CGFloat] = []
    var waitOrPlay = false
    var currently_direction = ""
    var step_status = 0
    var countPoints = 0
    var current_color = 1
    var network = false
    var current_point = 0
    var pasangerInCar = false
    var driverLeft_T = Timer()
    var driverTo_T = Timer()
    var timer_t = Timer()
    var status_id_t = Timer()
    var location_timer = Timer()
    var order_id = 0
    var driver_id = 0
    var latitude = 0.0
    var completeonClosure = 0.0
    var data_key = "data_from"
    var data_keyFrom = "data_from"
    var data_casual = 0
    var name = ""
    var dropDown = false
    var driverIsBusy = false
    var hours = 0
    var minute = 0
    var seconds = 0
    var diffHrs = 0
    var diffMins = 0
    var status_id = "2"
    var diffSecs = 0
    var currentRequest = 0
    var company_name = ""
    var locationManager = CLLocationManager()
    var driver_status_t = Timer()
    var channel : PusherChannel! = nil
    var  channel_update_location : PusherChannel! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        addsubviews()
        CreateConstraints()
        RestoreProgress()
        createPusher()
        getAcces()
        removeSavedDate()
        let frequency_in_order = UserDefaults.standard.integer(forKey: "frequency_in_order")
        print("afssafa",frequency_in_order/1000)
        self.location_timer = Timer.scheduledTimer(timeInterval:TimeInterval(frequency_in_order/1000), target: self, selector: (#selector(sendLocationToServer)), userInfo: nil, repeats: true)
        NotificationCenter.default.addObserver(self, selector: #selector(pauseWhenBackground(noti:)), name: .UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground(noti:)), name: .UIApplicationWillEnterForeground, object: nil)
    }
    @objc func pauseWhenBackground(noti: Notification) {
        let shared = UserDefaults.standard
        shared.set(Date(), forKey: "savedTime")
    }
    @objc func willEnterForeground(noti: Notification) {
        if let savedDate = UserDefaults.standard.object(forKey: "savedTime") as? Date {
            (diffHrs, diffMins, diffSecs) = MainViewController.getTimeDifference(startDate: savedDate)
            
            self.refresh(hours: diffHrs, mins: diffMins, secs: diffSecs)
        }
    }
    func refresh (hours: Int, mins: Int, secs: Int) {
        self.hours += hours
        self.minute += mins
        self.seconds += secs
        removeSavedDate()
    }
    static func getTimeDifference(startDate: Date) -> (Int, Int, Int) {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.hour, .minute, .second], from: startDate, to: Date())
        return(components.hour!, components.minute!, components.second!)
    }
    func removeSavedDate() {
        if (UserDefaults.standard.object(forKey: "savedTime") as? Date) != nil {
            UserDefaults.standard.removeObject(forKey: "savedTime")
        }
    }
    func getAcces () {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    func createPusher() {
        let pusherClientOptions = PusherClientOptions(authMethod: .inline(secret: "f288a464d3766c8a7fd6"),host: .cluster("ap2"))
        pusher = Pusher(key: "7ea5886145e8af112d5f", options: pusherClientOptions)
        pusher.delegate = self
        pusher.connect()
        channel_update_location = pusher.subscribe(channelName: "private-driver-location")
        NetworkManager.makeRequest(target: .getDriverStatus(driver_id: driver_id)) { (json) in
            let status_id = json["data"]["status_id"].stringValue
            if (status_id != "4"){
              //  self.orderDeleted()
            }
            if UserDefaults.standard.bool(forKey: "driverIsBusy") == true {
                self.status_id = "4"
            }
            else {
                self.status_id = "2"
            }
        }
        self.driver_status_t = Timer.scheduledTimer(timeInterval: 60, target: self, selector: (#selector(changeDriverStatus)), userInfo: nil, repeats: true)
        // subscribe to channel and bind to event
    }
    @objc func willShowLeft(notification: NSNotification) {
        self.webView.isUserInteractionEnabled = false
    }
    @objc func didHideLeft(notification: NSNotification) {
        self.webView!.isUserInteractionEnabled = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.webView!.isUserInteractionEnabled = true
        let notificationName = Notification.Name("push")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(newPush),
            name: notificationName,
            object: nil)
        let new_order_name = Notification.Name("new_order")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(new_orderPush),
            name: new_order_name,
            object: nil)
        let acceptOrdername = Notification.Name("neworder")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(acceptOrder),
            name: acceptOrdername,
            object: nil)
        let willShowLeftname = Notification.Name("willShowLeft")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(willShowLeft),
            name: willShowLeftname,
            object: nil)
        let didHideLeftname = Notification.Name("didHideLeft")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didHideLeft),
            name: didHideLeftname,
            object: nil)
        let finish_name = Notification.Name("finish")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(finishPush),
            name: finish_name,
            object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    @objc func finishPush(notification: NSNotification){
        resetOrder()
        SaveProgress()
    }
    func RestoreProgress () {
        if UserDefaults.standard.bool(forKey: "driverIsBusy") == true {
            inter_color.removeAll()
            status_id = "4"
            driverIsBusy = UserDefaults.standard.bool(forKey: "driverIsBusy")
            name = UserDefaults.standard.string(forKey: "passanger_name")!
            let order_id  = UserDefaults.standard.string(forKey: "order_id")!
            countPoints = UserDefaults.standard.integer(forKey: "countPoints")
            current_color = UserDefaults.standard.integer(forKey: "current_color")
            current_point = UserDefaults.standard.integer(forKey: "current_point")
            currentRequest = UserDefaults.standard.integer(forKey: "currentRequest")
            step_status = UserDefaults.standard.integer(forKey: "step_status")
            data_key = UserDefaults.standard.string(forKey: "data_key")!
            data_keyFrom = UserDefaults.standard.string(forKey: "data_keyFrom")!
            pasangerInCar = UserDefaults.standard.bool(forKey: "pasangerInCar")
            driver_id = UserDefaults.standard.integer(forKey: "driver_id")
            company_name = UserDefaults.standard.string(forKey: "company")!
            var userDefaults = UserDefaults.standard
            let decoded  = userDefaults.object(forKey: "inter_color") as! Data
            inter_color = (NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSArray) as! [UIColor]
            StartRestore(order_id:Int(order_id)!)
            let image: UIImage = UIImage(named: "menu")!
            menu_btn.setImage(image, for: .normal)
            self.status_id_t = Timer.scheduledTimer(timeInterval: MainViewController.time_interval, target: self, selector: (#selector(getDriverStatus)), userInfo: nil, repeats: true)
        }
    }
    func SaveProgress() {
        UserDefaults.standard.set(countPoints, forKey: "countPoints")
        UserDefaults.standard.set(current_color, forKey: "current_color")
        UserDefaults.standard.set(current_point, forKey: "current_point")
        UserDefaults.standard.set(data_key, forKey: "data_key")
        UserDefaults.standard.set(data_keyFrom, forKey: "data_keyFrom")
        UserDefaults.standard.set(currentRequest, forKey: "currentRequest")
        UserDefaults.standard.set(order[0].order_id, forKey: "order_id")
        UserDefaults.standard.set(driver_id, forKey: "driver_id")
        var userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: inter_color)
        userDefaults.set(encodedData, forKey: "inter_color")
        UserDefaults.standard.set(driverIsBusy, forKey: "driverIsBusy")
        UserDefaults.standard.set(step_status, forKey: "step_status")
        UserDefaults.standard.set(pasangerInCar, forKey: "pasangerInCar")
        UserDefaults.standard.set(company_name, forKey: "company")
        userDefaults.synchronize()
    }
    func StartRestore(order_id: Int){
        NetworkManager.makeRequest(target: .getNewOrder(order_id: order_id)){ [weak self] (json) in
            guard let vc = self else { return }
            vc.order.removeAll()
            vc.stepSizes.removeAll()
            vc.inter_color.removeAll()
            for (_,subJson):(String, JSON) in json {
                let new = OrderInfo(json: subJson)
                vc.order.append(new)
            }
            vc.stepSizes.append(80)
            vc.stepSizes.append(75)
            vc.inter_color.append(UIColor.black)
            vc.inter_color.append(UIColor.white)
            for (_,subJson):(String, JSON) in json["data"]["data_additional"] {
                let new = OrderInfo(json :subJson)
                vc.data_additional.append(new)
                vc.stepSizes.append(44)
                vc.inter_color.append(UIColor.lightGray)
                vc.currently_direction = vc.data_additional[0].longitude + "," + vc.data_additional[0].latitude
            }
            vc.stepSizes.append(34)
            vc.inter_color.append(UIColor.lightGray)
            vc.loadMap(latitude: "43.2377", longitude: "76.878204",order_id:"\(vc.order[0].order_id)", status : "drive")
            vc.driverIsBusy = true
            vc.status_sw.isHidden = true
            vc.switch_lb.isHidden = true
            vc.ready_btn.isHidden = false
            UIApplication.shared.isIdleTimerDisabled = true
            vc.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            vc.view.addSubview(vc.status_btn)
            vc.ConstraintsForWaitPlay()
            vc.addtimer()
            vc.twogGisNavigation()
            let size = vc.data_additional.count * 44
            vc.tv_height.constant = vc.tableView.frame.height + 200 + CGFloat(size)
            UIView.animate(withDuration: 0.3) {
                vc.view.layoutIfNeeded()
            }
            vc.inter_color[vc.current_color] = (UIColor.white)
            vc.inter_color[vc.current_color - 1] = (UIColor.lightGray)
            vc.tableView.reloadData()
            vc.whereCurrentRequest()
            // check current point
            print("testing restore",vc.step_status,vc.pasangerInCar)
            if (vc.step_status == 2) {
                    if (vc.pasangerInCar == false) {
                        vc.driverLeft_T.invalidate()
                        vc.ready_btn.setTitle("Пассажир на месте",for: .normal)
                        vc.status_lb.text = "Водитель ожидает пассажира"
                }
                    else {
                        if (vc.countPoints == -1) {
                            vc.complete_btn.setTitle("Завершить поездку",for: .normal)
                            vc.ready_btn.removeFromSuperview()
                            vc.complete_btn.isHidden = false
                            vc.status_lb.text = "Едем к месту назначения"
                        }
                        else {
                            vc.ready_btn.setTitle("Приехали к месту #\(vc.current_point+1)",for: .normal)
                            vc.status_lb.text = "Едем к месту назначения"
                        }
                    }
            
            }
            else if (vc.step_status == 3) {
                vc.ready_btn.setTitle("На месте",for: .normal)
                vc.status_lb.text = "Клиент ожидает машину"
            }
        }
    }
    @objc func newPush(notification: NSNotification){
        var json = JSON.parse(notification.object as! String)
        let order_id = json["order_id"].intValue
        print("afsasfafs",order_id)
        name = json["passanger_name"].stringValue
        company_name = json["company_name"].stringValue
        let status_id = json["status_id"].intValue
        UserDefaults.standard.set(name, forKey: "passanger_name")
        UserDefaults.standard.set(order_id, forKey: "order_id")
        UserDefaults.standard.set(company_name, forKey: "company")
        if (status_id == 4)||(status_id == 5)  {
            orderDeleted()
        }
        else if (status_id == 2)||(status_id == 3){
            if (!driverIsBusy){
                resetOrder()
                driverSelected(order_id: order_id)
            }
        }
        else {
            if (!driverIsBusy) {
                driverSelected(order_id:order_id)
            }
        }
    }
    private func resetOrder(){
        inter_color.removeAll()
        name = UserDefaults.standard.string(forKey: "passanger_name")!
        countPoints = 0
        current_color = 1
        current_point = 0
        currentRequest = 0
        step_status = 0
        data_key = "data_from"
        data_keyFrom = "data_from"
        pasangerInCar = false
        driver_id = UserDefaults.standard.integer(forKey: "driver_id")
        company_name = UserDefaults.standard.string(forKey: "company")!
    }
    @objc func new_orderPush(notification: NSNotification){
    }
    func orderDeleted() {
        UserDefaults.standard.set(false, forKey: "driverIsBusy")
        UserDefaults.standard.set(false, forKey: "additional_bool")
        UserDefaults.standard.set(false, forKey: "arrived")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_1 = storyboard.instantiateViewController(withIdentifier: "ordernavig")
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = vc_1
        SVProgressHUD.showSuccess(withStatus: BaseMessage.orderRemoved)
    }
    func driverSelected(order_id : Int ) {
        NetworkManager.makeRequest(target: .getNewOrder(order_id: order_id)){ [weak self] (json) in
            guard let vc = self else { return }
            vc.order.removeAll()
            vc.stepSizes.removeAll()
            vc.inter_color.removeAll()
            for (_,subJson):(String, JSON) in json {
                let new = OrderInfo(json: subJson)
                vc.order.append(new)
                //vc.getPassangerInfo(passanger_id: vc.order[0].passanger_id)
            }
            vc.loadMap(latitude: "43.2377", longitude: "76.878204",order_id: "\(vc.order_id)",status : "start")
            vc.stepSizes.append(80)
            vc.stepSizes.append(75)
            vc.inter_color.append(UIColor.black)
            vc.inter_color.append(UIColor.white)
            for (_,subJson):(String, JSON) in json["data"]["data_additional"] {
                let new = OrderInfo(json :subJson)
                vc.data_additional.append(new)
                vc.stepSizes.append(44)
                vc.inter_color.append(UIColor.lightGray)
                vc.currently_direction = vc.data_additional[0].longitude + "," + vc.data_additional[0].latitude
                vc.countPoints += 1
            }
            vc.stepSizes.append(34)
            vc.inter_color.append(UIColor.lightGray)
            vc.newOrder()
            vc.accept_order()
        }
    }
    @objc func acceptOrder(notification: NSNotification){
        if (!driverIsBusy) {
            var json = JSON.init(notification.object!)
            let order_id = json["id"].stringValue
            name = json["passanger_name"].stringValue
            company_name = json["company"].stringValue
            UserDefaults.standard.set(name, forKey: "passanger_name")
            UserDefaults.standard.set(order_id, forKey: "order_id")
            getOrderInfo(order_id:Int(order_id)!)
        }
        let image: UIImage = UIImage(named: "back")!
        menu_btn.setImage(image, for: .normal)
    }
    private func loadMap(latitude : String ,longitude: String , order_id : String, status : String) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestAlwaysAuthorization()
                locationManager.startUpdatingLocation()
            case .authorizedAlways, .authorizedWhenInUse:
                var myURL = URL(string: "")
                if (status == "drive"){
                    myURL =  URL(string: "\(BaseMessage.baseUrl)/map-driver?method=\(status)&order_id=\(order_id)&driver_id=\((UserDefaults.standard.string(forKey: "user_id")!))&from=\(data_keyFrom)&to=\(data_key)")
                }
                else {
                    let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
                    let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
                    myURL =  URL(string: "\(BaseMessage.baseUrl)/map-driver?method=\(status)&order_id=\(order_id)&driver_id=\((UserDefaults.standard.string(forKey: "user_id")!))&latitude=\(myLatitude)&longitude=\(myLongitude)")
                }
                let myRequest = URLRequest(url: myURL!)
                webView = WKWebView(frame: webView.frame)
                view.addSubview(webView)
                webView.navigationDelegate = self
                webView.load(myRequest)
                CreateConstraintsWebview()
                addsubviews()
                twogGisNavigation()
                ready_btn.isHidden = false
            }
        } else {
            SVProgressHUD.showError(withStatus:BaseMessage.locationdisabled)
            print("Location services are not enabled")
        }
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "status_id":status_id,
            "driver_id":(UserDefaults.standard.string(forKey: "user_id")),
            "location": savedData,
            "car_number":(UserDefaults.standard.string(forKey: "car_id")),
            ]
        print("asfasf",jsonObject)
        channel_update_location.trigger(eventName: "client-location-updated", data:jsonObject)
    
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
    @objc func sendLocationToServer(timer: Timer){
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "status_id":status_id,
            "driver_id":(UserDefaults.standard.string(forKey: "user_id")),
            "location": savedData,
            "car_number":(UserDefaults.standard.string(forKey: "car_id")),
            ]
        print("asfasf",jsonObject)
        channel_update_location.trigger(eventName: "client-location-updated", data:jsonObject)
    }
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let user = "dev@amotors.kz"
        let password = "123123"
        let credential = URLCredential(user: user, password: password, persistence: URLCredential.Persistence.forSession)
        challenge.sender?.use(credential, for: challenge)
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (step_status == 1){
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "info_cell", for: indexPath) as! InfoTableViewCell
                cell.selectionStyle = .none
                cell.contentView.backgroundColor = UIColor.white
                //cell.pre_info_lb.text  = "ФИО"
                cell.info_lb.text = "\(name)  (\(company_name))"
                let order_at = order[0].order_at
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm"
                
                if let date = dateFormatterGet.date(from: order_at){
                    cell.order_at_lb.text =  dateFormatterPrint.string(from: date)
                }
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "district_cell", for: indexPath) as! DistricTableViewCell
                cell.selectionStyle = .none
                cell.contentView.backgroundColor = UIColor.black
                cell.district.textColor = UIColor.white
                cell.setupData(order[0])
                return cell
                
            }
        }
        else {
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "info_cell", for: indexPath) as! InfoTableViewCell
                cell.selectionStyle = .none
                cell.contentView.backgroundColor = UIColor.white
              //  cell.pre_info_lb.text  = "ФИО"
                cell.info_lb.text = "\(name) (\(company_name))"
                let order_at = order[0].order_at
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm"
                
                if let date = dateFormatterGet.date(from: order_at){
                    cell.order_at_lb.text =  dateFormatterPrint.string(from: date)
                }
                return cell
                
            }
            else if (indexPath.row == 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "begindistrict_cell", for: indexPath) as! BeginDistrictTableViewCell
                cell.selectionStyle = .none
                cell.contentView.backgroundColor = UIColor.black
                // cell.district.textColor = BaseMessage.textcolor
                let image: UIImage = UIImage(named: "intermediate_icon")!
                cell.icon.image = image
                cell.district_lb.text = order[0].data_from
                cell.district_lb.textColor = inter_color[indexPath.row]
                cell.drop_down_btn.addTarget(self, action: #selector(dropDownBtn), for: .touchUpInside)
                if (data_additional.count == 0){
                    cell.drop_down_btn.isHidden = true
                }
                return cell
            }
            else if indexPath.row == 2+data_additional.count{
                let cell = tableView.dequeueReusableCell(withIdentifier: "begin_cell", for: indexPath) as! BeginFinelPointsTableViewCell
                cell.selectionStyle = .none
                cell.contentView.backgroundColor = UIColor.black
                // cell.district.textColor = BaseMessage.textcolor
                cell.district.text = order[0].data_to
                cell.district.textColor = inter_color[indexPath.row]
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "order_cell", for: indexPath) as! OrderDistrictTableviewCell
                cell.selectionStyle = .none
                cell.contentView.backgroundColor = UIColor.black
                cell.district_lb.textColor = inter_color[indexPath.row]
                cell.district_lb.text = data_additional[indexPath.row-2].name
                return cell
                
            }
            
        }
        
    }
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if step_status == 1 {
            return 2
        }
        else if  step_status == 0 {
            return 0
        }
        else {
            return stepSizes.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch step_status {
        case 1 :
            if (indexPath.row == 0){
                return 80
            }
            else {
                return 60
            }
        case 2,3:
            if (stepSizes.count == 0){
                return 60
            }
            else {
            return stepSizes[indexPath.row]
        }
        default :
            return 60
        }
    }
    func updateUserInterface() {
        guard let status = Network.reachability?.status else { return }
      //  switch status {
       /* case .unreachable:
            view.backgroundColor = .red
        case .wifi:
            view.backgroundColor = .green
        case .wwan:
            view.backgroundColor = .yellow
        }*/
        network = (Network.reachability?.isReachable)!
        print("Reachability Summary",network)
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
        if (network == false){
             SVProgressHUD.showError(withStatus: BaseMessage.internet_error)
        }
    }
   @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    func configUI() {
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        updateUserInterface()
        // fix for iphone x , i like the phone(no)
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height != 2436 {
            tableViewTop.constant = -20
            tv_height.constant = tableView.frame.height - 13
            self.view.layoutIfNeeded()
        }
        else {
        }
        
        status_sw.setOn(UserDefaults.standard.bool(forKey: "status_sw"), animated: false)
        if UserDefaults.standard.bool(forKey: "status_sw") == true{
            switch_lb.text = "Готов"
        }
        else {
            switch_lb.text = "Не доступен"
        }
        
        // Equivalent to
        // let dynamicButton   = DynamicButton()
        // dynamicButton.style = .hamburger
        
        // Animate the style update
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "DistricTableViewCell", bundle: nil), forCellReuseIdentifier: "district_cell")
        tableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "info_cell")
        tableView.register(UINib(nibName: "DistanceTableViewCell", bundle: nil), forCellReuseIdentifier: "distance_cell")
        tableView.register(UINib(nibName: "OrderDistrictTableviewCell", bundle: nil), forCellReuseIdentifier: "order_cell")
        tableView.register(UINib(nibName: "BeginFinelPointsTableViewCell", bundle: nil), forCellReuseIdentifier: "begin_cell")
        tableView.register(UINib(nibName: "BeginDistrictTableViewCell", bundle: nil), forCellReuseIdentifier: "begindistrict_cell")
        status_sw.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
    }
    func addsubviews()
    {
        view.addSubview(ready_btn)
        view.addSubview(complete_btn)
        complete_btn.isHidden = true
        ready_btn.isHidden = true
    }
    func CreateConstraintsWebview(){
        //constatraints for webview
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        webView.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 0).isActive = true
    }
    
    func CreateConstraints()  {
        // constraints for ready button
        ready_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        ready_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            ready_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
        }
        else {
            ready_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40).isActive = true
        }
        ready_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        // constraints for complete button
        complete_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        complete_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            complete_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
        }
        else {
            complete_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40).isActive = true
        }
        complete_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    func ConstraintsForWaitPlay()  {
        // constraints for status button
        status_btn.leftAnchor.constraint(equalTo: status_sw.leftAnchor, constant: 18).isActive = true
        status_btn.rightAnchor.constraint(equalTo: status_sw.rightAnchor, constant: -17).isActive = true
        status_btn.bottomAnchor.constraint(equalTo: status_sw.bottomAnchor, constant: -20).isActive = true
        status_btn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        status_btn.widthAnchor.constraint(equalToConstant: status_btn.frame.width).isActive = false
        
    }
    func ConstraintsForTwoGisNavigation()  {
        // constraints for status button
        twogis_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        twogis_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            twogis_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -34).isActive = true
        }
        else {
            twogis_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        twogis_btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        RemoveFromSubView()
        view.layoutIfNeeded()
        
    }
    func RemoveFromSubView()  {
        complete_btn.removeFromSuperview()
        view.addSubview(complete_btn)
        
        complete_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        complete_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        complete_btn.bottomAnchor.constraint(equalTo: twogis_btn.topAnchor, constant: -10).isActive = true
        complete_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        ready_btn.removeFromSuperview()
        view.addSubview(ready_btn)
        
        ready_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        ready_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        ready_btn.bottomAnchor.constraint(equalTo: twogis_btn.topAnchor, constant: -10).isActive = true
        ready_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
    }
    let timer_lb: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "0"
        view.textAlignment = .center
        view.textColor = UIColor.white
        return view
    }()
    let ready_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("ГОТОВ",for: .normal)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(ready_btn_clicked), for: .touchUpInside)
        return view
    }()
    let complete_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Отказаться от заявки",for: .normal)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(complete_btn_clicked), for: .touchUpInside)
        return view
    }()
    let call_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "call")!
        view.setImage(image, for: .normal)
        return view
    }()
    let status_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("В пути",for: .normal)
        view.contentHorizontalAlignment = .center
        view.contentVerticalAlignment = .fill
        view.titleLabel?.font = view.titleLabel?.font.withSize(12)
        view.titleEdgeInsets.top = 50
        view.titleEdgeInsets.bottom = 0
        view.titleEdgeInsets.right = 30
        view.titleEdgeInsets.left = 30
        var image: UIImage = UIImage(named: "pause")!
        view.setImage(image, for: .normal)
        view.addTarget(self, action: #selector(status_btn_clicked), for: .touchUpInside)
        return view
    }()
    let twogis_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "2gisnavigation")!
        view.setImage(image, for: .normal)
        view.addTarget(self, action: #selector(twogisnivation_clicked), for: .touchUpInside)
        return view
    }()
    //API
    func getPassangerInfo(passanger_id : Int){
        NetworkManager.makeRequest(target: .getPassengerInfo(id: passanger_id)){ [weak self] (json) in
            guard let vc = self else { return }
            vc.passanger.removeAll()
            for (_,subJson):(String, JSON) in json {
                let new = Passanger(json: subJson)
                vc.passanger.append(new)
            }
            vc.tableView.reloadData()
        }
    }
    @objc func dropDownBtn(_ sender: AnyObject?) {
        if (!dropDown) {
            let size = data_additional.count * 44
            tv_height.constant = tableView.frame.height - CGFloat(size) - 40
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            dropDown = true
        }
        else {
            let size = data_additional.count * 44
            tv_height.constant = tableView.frame.height + CGFloat(size) + 40
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            dropDown = false
        }
    }
    func getOrderInfo(order_id : Int){
        NetworkManager.makeRequest(target: .getNewOrder(order_id: order_id)){ [weak self] (json) in
            guard let vc = self else { return }
            vc.order.removeAll()
            vc.stepSizes.removeAll()
            vc.inter_color.removeAll()
            for (_,subJson):(String, JSON) in json {
                let new = OrderInfo(json: subJson)
                vc.order.append(new)
                //vc.getPassangerInfo(passanger_id: vc.order[0].passanger_id)
            }
            vc.loadMap(latitude: "43.2377", longitude: "76.878204",order_id: "\(order_id)",status : "start")
            vc.stepSizes.append(80)
            vc.stepSizes.append(75)
            vc.inter_color.append(UIColor.black)
            vc.inter_color.append(UIColor.white)
            for (_,subJson):(String, JSON) in json["data"]["data_additional"] {
                let new = OrderInfo(json :subJson)
                vc.data_additional.append(new)
                vc.stepSizes.append(44)
                vc.inter_color.append(UIColor.lightGray)
                vc.currently_direction = vc.data_additional[0].longitude + "," + vc.data_additional[0].latitude
                vc.countPoints += 1
            }
            vc.stepSizes.append(34)
            vc.inter_color.append(UIColor.lightGray)
            vc.newOrder()
        }
    }
    func newOrder() {
        self.tv_height.constant = self.tableView.frame.height + 140
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        step_status = 1
        ready_btn.setTitle("Принять заказ",for: .normal)
        ready_btn.isHidden = false
        status_lb.text = "Клиент ожидает машину"
        tableView.reloadData()
    }
    @objc func ready_btn_clicked(_ sender: AnyObject?) {
        if (network == true){
        if (step_status == 1){
            accept_order()
        }
        else  if (step_status == 3){
            passangerInPlace()
            driverLeft_T.invalidate()
            driverArrived(order_id:order[0].order_id, driver_id: driver_id){
                returnJSON in
            }
            ready_btn.setTitle("Пассажир на месте",for: .normal)
            status_lb.text = "Водитель ожидает пассажира"
            step_status = 2
            SaveProgress()
            print("passangerInPlace")
        }
        else {
            if (countPoints == 0){
                InPlace()
            }
            else {
                singleDriverTo()
                nextpoint()
            }

            if (pasangerInCar == false) {
                self.pasangerInCar = true
                passangerPicked(order_id:order[0].order_id, driver_id: driver_id){
                    returnJSON in
                    print(returnJSON)
                }
                var local_data_key = ""
                if (countPoints == -1){
                    local_data_key = "data_to"
                    data_key = local_data_key
                }
                else {
                    local_data_key = "data_additional_\(current_color-1)"
                }
                driveTo(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude:0,data_key :local_data_key ){
                    returnJSON in
                    print(returnJSON)
                }
                data_keyFrom = "data_from"
                self.loadMap(latitude: "43.2377", longitude: "76.878204",order_id:"\(self.order[0].order_id)", status : "drive")
                if (countPoints == -1){
                complete_btn.setTitle("Завершить поездку",for: .normal)
                ready_btn.removeFromSuperview()
                complete_btn.isHidden = false
                }
            }
            else {
                var local_data_key = ""
                if (countPoints == -1){
                    local_data_key = "data_to"
                    data_key = local_data_key
                }
                else {
                    local_data_key = "data_additional_\(current_color-1)"
                }
                driveTo(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude:0,data_key :local_data_key ){
                    returnJSON in
                    print(returnJSON)
            }
            }
            SaveProgress()
        }
        }
        else {
             SVProgressHUD.showError(withStatus: BaseMessage.internet_error)
        }
    }
    @objc func complete_btn_clicked(_ sender: AnyObject?) {
        complete_trip()
    }
    func nextpoint() {
        if (pasangerInCar == true) {
            step_status = 2
            current_point += 1
            current_color += 1
            ready_btn.setTitle("Приехали к месту #\(current_point+1)",for: .normal)
            status_lb.text = "Едем к месту назначения"
            data_keyFrom = data_key
            data_key = "data_additional_\(current_point)"
            self.loadMap(latitude: "43.2377", longitude: "76.878204",order_id:"\(self.order[0].order_id)", status : "drive")
            arrivedTo(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude: 0, data_key: data_key){
                returnJSON in
                print(returnJSON)
            }
            data_key = "data_additional_\(current_point+1)"
            countPoints -= 1
            inter_color[current_color] = (UIColor.white)
            inter_color[current_color - 1] = (UIColor.lightGray)
            SaveProgress()
            tableView.reloadData()
        }
        else {
            step_status = 2
            current_color += 1
            countPoints -= 1
            ready_btn.setTitle("Приехали к месту #\(current_point+1)",for: .normal)
            status_lb.text = "Едем к месту назначения"
            inter_color[current_color] = (UIColor.white)
            inter_color[current_color - 1] = (UIColor.lightGray)
            SaveProgress()
            tableView.reloadData()
        }
    }
    func InPlace(){
        step_status = 2
        current_color += 1
        data_keyFrom = data_key
        if (countPoints != 0){
            arrivedTo(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude: 0, data_key: data_key){
                returnJSON in
                print(returnJSON)
            }
        }
        if data_key == "data_additional_\(current_point+1)"{
            arrivedTo(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude: 0, data_key: data_key){
                returnJSON in
                print(returnJSON)
            }
        }
        data_key = "data_to"
        self.loadMap(latitude: "43.2377", longitude: "76.878204",order_id:"\(self.order[0].order_id)", status : "drive")
        countPoints -= 1
        inter_color[current_color] = (UIColor.white)
        inter_color[current_color - 1] = (UIColor.lightGray)
        complete_btn.setTitle("Завершить поездку",for: .normal)
        ready_btn.removeFromSuperview()
        complete_btn.isHidden = false
        status_lb.text = "Едем к месту назначения"
        SaveProgress()
        tableView.reloadData()
        print("working,zavershit")
    }
    func passangerInPlace(){
        driverLeft_T.invalidate()
        driverArrived(order_id:order[0].order_id, driver_id: driver_id){
            returnJSON in
        }
        ready_btn.setTitle("Пассажир на месте",for: .normal)
        status_lb.text = "Водитель ожидает пассажира"
        SaveProgress()
        step_status = 2
    }
    func accept_order(){
        if (data_additional.count >= 1){
            UserDefaults.standard.set(true, forKey: "additional_bool")
            let force_finish = Notification.Name("force_finish_add")
            NotificationCenter.default.post(name: force_finish, object: nil)
        }
        self.loadMap(latitude: "0", longitude: "0",order_id:"\(self.order[0].order_id)", status : "drive")
        let image: UIImage = UIImage(named: "menu")!
        menu_btn.setImage(image, for: .normal)
        step_status = 3
        driverIsBusy = true
        switch_lb.isHidden = true
        status_sw.isHidden = true
        ready_btn.isHidden = false
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        view.addSubview(status_btn)
        ConstraintsForWaitPlay()
        addtimer()
        twogGisNavigation()
        status_id = "4"
        UIApplication.shared.isIdleTimerDisabled = true
        ready_btn.setTitle("На месте",for: .normal)
        status_lb.text = "Клиент ожидает машину"
        tableView.reloadData()
        let size = data_additional.count * 44
        tv_height.constant = tableView.frame.height + 62 + CGFloat(size)
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if let driver = userid{
            NetworkManager.makeRequest(target: .acceptOrder(order_id:order[0].order_id, user_id:driver)){ [weak self] (json) in
                self?.status_id_t = Timer.scheduledTimer(timeInterval: MainViewController.time_interval, target: MainViewController(), selector: (#selector(MainViewController.getDriverStatus)), userInfo: nil, repeats: true)
                let code = json["code"].stringValue
                if (code == "302"){
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc_1 = storyboard.instantiateViewController(withIdentifier: "ordernavig")
                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window!.rootViewController = vc_1
                    SVProgressHUD.showSuccess(withStatus: BaseMessage.unFortune)
                }
                else {
                    self?.SaveProgress()
                }
                guard let vc = self else { return }
            }
            driver_id = driver
        }
        singleDriverLeft()
        driverLeft(order_id:order[0].order_id, driver_id: 1, longitude: 0, latitude: 0){
            returnJSON in
            print(returnJSON)
        }
        //   wait()
    }
    func complete_trip(){
        disableTimer()
        pusher.disconnect()
        if UserDefaults.standard.bool(forKey: "arrived") == false {
            arrivedTo(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude: 0, data_key: data_key){
                returnJSON in
                print(returnJSON)
            }
            UserDefaults.standard.set(true, forKey: "arrived")
        }
        SVProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.performSegue(withIdentifier: "complete", sender: nil)
        }
    }
    func twogGisNavigation(){
        view.addSubview(twogis_btn)
        ConstraintsForTwoGisNavigation()
    }
    func addtimer() {
        view.addSubview(timer_lb)
        timer_lb.leftAnchor.constraint(equalTo: status_btn.leftAnchor, constant: -10).isActive = true
        timer_lb.rightAnchor.constraint(equalTo: status_btn.rightAnchor, constant: 10).isActive = true
        timer_lb.bottomAnchor.constraint(equalTo: status_btn.bottomAnchor, constant: 40).isActive = true
        timer_lb.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    @objc func status_btn_clicked(_ sender: AnyObject?) {
        if (waitOrPlay == true){
            waitOrPlay = false
            addtimer()
            wait()
        }
        else {
            waitOrPlay = true
            start_trip()
            play()
        }
    }
    func start_trip (){
        step_status = 2
        status_lb.text = "Едем к месту назначения"
        tableView.reloadData()
    }
    func continueTrip() {
        whereCurrentRequest()
    }
    func wait(){
        status_lb.text = "Едем к месту назначения"
        status_btn.setTitle("В пути",for: .normal)
        let image: UIImage = UIImage(named: "pause")!
        status_btn.setImage(image, for: .normal)
        status_btn.titleEdgeInsets.right = 28
        status_btn.titleEdgeInsets.left = 45
        passangerBack(order_id:order[0].order_id, driver_id:driver_id,data_key :"data_casual_\(data_casual)"){
            returnJSON in
            print(returnJSON)
        }
        minute = 0
        seconds = 0
        ready_btn.isUserInteractionEnabled = true
        complete_btn.isUserInteractionEnabled = true
        timer_t.invalidate()
        self.continueTrip()
    }
    @objc func update() {
        if (seconds >= 59){
            minute += 1
            seconds = 1
            if (minute >= 60) {
                self.hours += 1
                minute = 0
            }
            self.timer_lb.text = "\(minute)" + "\(seconds)"
        }
        else {
            seconds += 1
            self.timer_lb.text = "\(minute):" + "\(seconds)"
        }
    }
    func play(){
        status_lb.text = "Ожидание клиента"
        status_btn.setTitle("Ожидание",for: .normal)
        let image: UIImage = UIImage(named: "play")!
        status_btn.setImage(image, for: .normal)
        status_btn.titleEdgeInsets.top = 50
        status_btn.titleEdgeInsets.right = 40
        status_btn.titleEdgeInsets.left = 40
        data_casual += 1
        passangerLeft(order_id:order[0].order_id, driver_id: driver_id,data_key :"data_casual_\(data_casual)"){
            returnJSON in
            print(returnJSON)
        }
        disableTimer()
        ready_btn.isUserInteractionEnabled = false
        complete_btn.isUserInteractionEnabled = false
        timer_t = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    @objc func twogisnivation_clicked(_ sender: AnyObject?) {
        let canOpenUrl = URL(string: "dgis://")
        var url = URL(string: "")
        if (step_status == 3){
            url = URL(string: "dgis://2gis.ru/routeSearch/rsType/car/to/\( order[0].longitude_from + "," + order[0].latitude_from)")
        }
        else if (step_status == 1 ){
            url = URL(string: "dgis://2gis.ru/routeSearch/rsType/car/to/\( order[0].longitude_from + "," + order[0].latitude_from)")
        }
        else {
            print("121212","121221" , current_color)
            if (countPoints == -1) || (countPoints == 0){
                url = URL(string: "dgis://2gis.ru/routeSearch/rsType/car/to/\( order[0].longitude_to + "," + order[0].latitude_to)")
            }
            else {
                if (current_color == 1){
                     print("121212","121221 minus 1" , current_color)
                    url = URL(string: "dgis://2gis.ru/routeSearch/rsType/car/to/\( data_additional[current_color - 1].longitude + "," + data_additional[current_color - 1].latitude)")
                }
                else {
                    print("121212"+"121221 minus 2" , current_color)
                    url = URL(string: "dgis://2gis.ru/routeSearch/rsType/car/to/\( data_additional[current_color - 2].longitude + "," + data_additional[current_color - 2].latitude)")
                }
            }
        }
        if UIApplication.shared.canOpenURL(canOpenUrl!){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
            }
        }
        else {
            let itunes = URL(string: "https://itunes.apple.com/ru/app/id481627348?mt=8")
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(itunes!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(itunes!)
            }
        }
    }
    @IBAction func menu_btn(_ sender: Any) {
        if (menu_btn.imageView?.image == UIImage(named: "menu")!){
            revealViewController()?.revealLeftView()
        }
        else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    @objc func switchValueDidChange(_ sender: UISwitch) {
        /*  var getStatus_sw = 0
         let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
         let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
         if status_sw.isOn == true {
         //  getOrderInfo(order_id:31)
         getStatus_sw = 1
         switch_lb.text = "Готов"
         UserDefaults.standard.set(true, forKey: "status_sw")
         }
         else {
         UserDefaults.standard.set(false, forKey: "status_sw")
         getStatus_sw = 2
         switch_lb.text = "Не доступен"
         if (step_status != 0) {
         step_status = 0
         ready_btn.setTitle("Принять заказ",for: .normal)
         self.tv_height.constant = self.tableView.frame.height-140
         UIView.animate(withDuration: 0.3) {
         self.view.layoutIfNeeded()
         }
         ready_btn.isHidden = true
         status_lb.text = "Местонахождение водителя"
         tableView.reloadData()
         }
         }
         NetworkManager.makeRequest(target: .changeDriverStatus(status_id: getStatus_sw, user_id: userid!,longitude:myLongitude, latitude :myLatitude )){ [weak self]
         (json) in
         guard self != nil else { return }
         }
         */
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "complete" {
            let destination: TravelDetailsController = segue.destination as! TravelDetailsController
            destination.orderid = order[0].order_id
            destination.driver_id = driver_id
        }
    }
    // getting where request
    func whereCurrentRequest() {
        if (currentRequest == 1){
            driverLeft(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude: 0){
                returnJSON in
                print(returnJSON)
            }
        }
        else if (currentRequest == 2){
            driveTo(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude: 0, data_key: data_key){
                returnJSON in
                print(returnJSON)
                
            }
        }
        else if (currentRequest == 3){
            arrivedTo(order_id:order[0].order_id, driver_id: driver_id, longitude: 0, latitude: 0, data_key: data_key){
                returnJSON in
                print(returnJSON)
                
            }
        }
    }
    
    
    
    
    
    
    
    ///
    
    
    
    
    
    
    //////Водитель приехал к месту забора
    func driverLeft(order_id:Int,driver_id:Int,longitude:Float,latitude:Float,completeonClosure: @escaping (AnyObject?) -> ()) {
        currentRequest = 1
        SaveProgress()
        self.driverLeft_T = Timer.scheduledTimer(timeInterval: MainViewController.time_interval, target: self, selector: (#selector(driverLeftTimer)), userInfo: nil, repeats: true)
    }
    //Водитель едет к месту назначения
    func driveTo(order_id:Int,driver_id:Int,longitude:Float,latitude:Float,data_key:String , completeonClosure: @escaping (AnyObject?) -> ()) {
        currentRequest = 2
        SaveProgress()
        self.data_key = data_key
        print("testing wrong driving",data_key)
        self.driverTo_T = Timer.scheduledTimer(timeInterval: MainViewController.time_interval, target: self, selector: (#selector(driverToTimer)), userInfo: nil, repeats: true)
        
    }
    //  Приехал к месту назначения
    func arrivedTo(order_id:Int,driver_id:Int,longitude:Float,latitude:Float ,data_key:String , completeonClosure: @escaping (AnyObject?) -> ()) {
        SaveProgress()
        self.data_key = data_key
        print("testing additional",data_key)
        NetworkManager.makeRequest(target: .arrivedTo(order_id: order[0].order_id, driver_id: driver_id,data_key: data_key, longitude: Float((locationManager.location?.coordinate.longitude)!), latitude: Float((locationManager.location?.coordinate.latitude)!))) { (json) in
        }
    }
    //Пассажир вернулся в машину
    func driverArrived(order_id:Int,driver_id:Int, completeonClosure: @escaping (AnyObject?) -> ()) {
        NetworkManager.makeRequest(target: .driverArrived(order_id:order_id,driver_id:driver_id, longitude: Float((locationManager.location?.coordinate.longitude)!), latitude: Float((locationManager.location?.coordinate.latitude)!))) { (json) in
            completeonClosure(json as AnyObject)
        }
    }
    //Пассажир сел в машину
    func passangerPicked(order_id:Int,driver_id:Int, completeonClosure: @escaping (AnyObject?) -> ()) {
        NetworkManager.makeRequest(target: .passangerPicked(order_id:order_id,driver_id:driver_id)) { (json) in
            completeonClosure(json as AnyObject)
            print("passanger picked")
              self.singleDriverTo()
        }
    }
    //Пассажир вышел из машину
    func passangerLeft(order_id:Int,driver_id:Int,data_key:String , completeonClosure: @escaping (AnyObject?) -> ()) {
        NetworkManager.makeRequest(target: .passangerLeft(order_id:order_id,driver_id:driver_id,data_key: data_key, longitude: Float((locationManager.location?.coordinate.longitude)!), latitude: Float((locationManager.location?.coordinate.latitude)!))) { (json) in
            completeonClosure(json as AnyObject)
        }
    }

    //Пассажир вернулся в машину
    func passangerBack(order_id:Int,driver_id:Int,data_key:String , completeonClosure: @escaping (AnyObject?) -> ()) { NetworkManager.makeRequest(target: .passangerBack(order_id:order_id,driver_id:driver_id,data_key:data_key,longitude: Float((locationManager.location?.coordinate.longitude)!), latitude: Float((locationManager.location?.coordinate.latitude)!))) { (json) in
        completeonClosure(json as AnyObject)
        }
    }
    // timers event
    @objc func driverLeftTimer(timer: Timer) {
        print ("driverLeftTimer")
        NetworkManager.makeRequest(target: .driverLeft(order_id: order[0].order_id, driver_id: driver_id, longitude: Float((locationManager.location?.coordinate.longitude)!), latitude: Float((locationManager.location?.coordinate.latitude)!))) { (json) in
        }
        //  timer.invalidate()
    }
    @objc func driverToTimer(timer: Timer) {
        print ("driverToTimer",data_key)
        NetworkManager.makeRequest(target: .driveTo(order_id: order[0].order_id, driver_id: driver_id,data_key: data_key, longitude: Float((locationManager.location?.coordinate.longitude)!), latitude: Float((locationManager.location?.coordinate.latitude)!))) { (json) in
        }
        //  timer.invalidate()
    }
    func singleDriverTo(){
        NetworkManager.makeRequest(target: .driveTo(order_id: order[0].order_id, driver_id: driver_id,data_key: data_key, longitude: Float((locationManager.location?.coordinate.longitude)!), latitude: Float((locationManager.location?.coordinate.latitude)!))) { (json) in
        }
    }
    func singleDriverLeft(){
        NetworkManager.makeRequest(target: .driverLeft(order_id: order[0].order_id, driver_id: driver_id, longitude: Float((locationManager.location?.coordinate.longitude)!), latitude: Float((locationManager.location?.coordinate.latitude)!))) { (json) in
        }
    }
    @objc func getDriverStatus(timer: Timer) {
        if UserDefaults.standard.bool(forKey: "driverIsBusy") == true {
            NetworkManager.makeRequest(target: .getDriverStatus(driver_id: userid!)) { (json) in
                let status_id = json["data"]["status_id"].stringValue
                if (status_id != "4"){
                   //  self.orderDeleted()
                   // self.disableTimer()
                   //self.status_id_t.invalidate()
                   // UserDefaults.standard.set(false, forKey: "additional_bool")
                }
            }
        }
    }
    @objc func changeDriverStatus(timer: Timer) {
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let status_local:Int? = Int(status_id)
        NetworkManager.makeRequest(target: .changeDriverStatus(status_id: status_local ?? 2, user_id: userid!,longitude:myLongitude, latitude :myLatitude )){ [weak self]
            (json) in
            guard self != nil else { return }
        }
    }
    func disableTimer() {
        driverLeft_T.invalidate()
        driverTo_T.invalidate()
        driver_status_t.invalidate()
        //  timer_t.invalidate()
    }
}

