 //
//  HistoryChildTableViewController.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 11.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
class HistoryChildTableViewController: UITableViewController {
    var limit = ""
    var history: [OrderHistory] = []
    var data_additional: [OrderHistory] = []
    var section_controller: [Bool] = []
    var Information: [OrderInformation] = []
    var time_total: [TimeTotal] = []
    var objectArray = [Objects]()
    var parentNavigationController : UINavigationController?
    let  userid = Int(UserDefaults.standard.string(forKey: "user_id")!)
    struct Objects {
        var sectionName : String!
        var sectionObjects : [OrderHistory]!
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        loadData()
    }
    func loadData() {
        SVProgressHUD.show()
        NetworkManager.makeRequest(target: .history(limit: limit, driver_id: userid!)) { [weak self] (json) in
            guard let vc = self else { return }
            vc.history.removeAll()
            vc.data_additional.removeAll()
            for (index,subJson):(String, JSON) in json["data"] {
                let new = OrderHistory(json: subJson)
                vc.history.append(new)
                for (_,subJson):(String, JSON) in  subJson["data_additional"]{
                    let new = OrderHistory(json :subJson)
                    vc.data_additional.append(new)
                }
                vc.objectArray.append(Objects(sectionName: "", sectionObjects: vc.data_additional))
                vc.data_additional.removeAll()
                for var i in (0..<subJson["information"]["exists"].count)
                {
                    i += 1
                    let Information = OrderInformation(json :subJson["information"]["exists"]["data_additional_\(i)"]["stats"])
                    if  subJson["information"]["exists"]["data_additional_\(i)"] != nil
                    {
                    vc.Information.append(Information)
                    }
                    
                }
                let Information = OrderInformation(json :subJson["information"]["exists"]["data_to"]["stats"])
                
                vc.Information.append(Information)
                let total = TimeTotal(json :subJson["information"]["stats"])
                vc.time_total.append(total)
                if (subJson["data_additional"] != nil) {
                    vc.section_controller.append(true)
                }
                    SVProgressHUD.dismiss()
            }
            vc.tableView.delegate = self
            vc.tableView.dataSource = self
            vc.tableView.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray
        
        let headerLabel = UILabel(frame: CGRect(x: 30, y: 10, width:
        tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "Verdana", size: 20)
        headerLabel.textColor = UIColor.white
        headerLabel.text = history[section].title
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let notificationName = Notification.Name("detail_history")
    NotificationCenter.default.post(name: notificationName, object: history[indexPath.section].history_id)
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return history.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section_controller.count-1 >= section {
            return objectArray[section].sectionObjects.count + 2
        }
        else {
            return 1
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if section_controller.count <= indexPath.section  {
         return 127
        }
        else {
            if (indexPath.row == 0){
             return 94
            }
            else if (indexPath.row == objectArray[indexPath.section].sectionObjects.count + 1){
                
            return 90
            }
            else {
             return 58
            }
        }
    
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if section_controller.count-1 >= indexPath.section {
            if (indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "first_history_cell", for: indexPath) as! HistoryAdditionalFirstTableViewCell
                let started_at = history[indexPath.section].started_at
                let finished_at = history[indexPath.section].finished_at
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "HH:mm"
                
                let dateFormatteryear = DateFormatter()
                dateFormatteryear.dateFormat = "dd-MM-yyyy"
                if let date = dateFormatterGet.date(from: started_at){
                    cell.timeStart_lb.text = dateFormatterPrint.string(from: date) + "-"
                }
                if let date = dateFormatterGet.date(from: finished_at){
                    cell.timeEnd_lb.text = dateFormatterPrint.string(from: date)
                }
                if let date = dateFormatterGet.date(from: started_at){
                    cell.year_lb.text = dateFormatteryear.string(from: date)
                }
                cell.districtFrom_lb.text = history[indexPath.section].data_fromName
                if Information[indexPath.section].time > 60 {
                    cell.distance_lb.text = "\(Information[indexPath.row].distance/1000) км(\(Information[indexPath.section].time / 60) мин)"
                }
                else {
                    cell.distance_lb.text = "\(Information[indexPath.row].distance/1000) км(\(Information[indexPath.section].time) сек)"
                }
                return cell
                
            }
            else if (indexPath.row == objectArray[indexPath.section].sectionObjects.count + 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "historylast_cell", for: indexPath) as! HistoryAdditionalLastTableViewCell
                cell.title_lb.text = history[indexPath.section].data_toName
                cell.totalStats_lb.text = "Время в пути \(time_total[indexPath.section].time_drive)сек,время ожидания \(time_total[indexPath.section].time_wait) минут ,расстояние \(time_total[indexPath.section].distance/1000)км"
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "historyadd_cell", for: indexPath) as! HistoryAdditionalTableViewCell
                cell.title_lb.text = objectArray[indexPath.section].sectionObjects[indexPath.section - 1].name
                if Information[indexPath.section].time > 60 {
                cell.distance_lb.text = "\(Information[indexPath.section].distance/1000) км(\(Information[indexPath.section].time / 60) мин)"
                }
                else {
                cell.distance_lb.text = "\(Information[indexPath.section].distance/1000) км(\(Information[indexPath.section].time) сек)"
                }
                return cell
            }
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "history_cell", for: indexPath) as! HistoryTableViewCell
            let started_at = history[indexPath.section].started_at
            let finished_at = history[indexPath.section].finished_at
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "HH:mm"
            
            let dateFormatteryear = DateFormatter()
            dateFormatteryear.dateFormat = "dd-MM-yyyy"
            if let date = dateFormatterGet.date(from: started_at){
                cell.timeStart_lb.text = dateFormatterPrint.string(from: date) + "-"
            }
            if let date = dateFormatterGet.date(from: finished_at){
                cell.timeEnd_lb.text = dateFormatterPrint.string(from: date)
            }
            
            if let date = dateFormatterGet.date(from: started_at){
                cell.year_lb.text = dateFormatteryear.string(from: date)
            }
            if Information[indexPath.section].time > 60 {
                cell.distance_lb.text = "\(Information[indexPath.section].distance/1000) км(\(Information[indexPath.section].time / 60) мин)"
            }
            else {
                cell.distance_lb.text = "\(Information[indexPath.section].distance/1000) км(\(Information[indexPath.section].time) сек)"
            }
        
            cell.districtFrom_lb.text = history[indexPath.section].data_fromName
            cell.districtTo_lb.text = history[indexPath.section].data_toName
            return cell
        }
    }
    func configUI() {
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.register(UINib(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "history_cell")
        tableView.register(UINib(nibName: "HistoryAdditionalFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "first_history_cell")
        tableView.register(UINib(nibName: "HistoryAdditionalTableViewCell", bundle: nil), forCellReuseIdentifier: "historyadd_cell")
        tableView.register(UINib(nibName: "HistoryAdditionalLastTableViewCell", bundle: nil), forCellReuseIdentifier: "historylast_cell")
    }
}
