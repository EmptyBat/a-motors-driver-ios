//
//  TravelHistoryController.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 10.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SDWebImage
import PageMenu
class TravelHistoryController: UIViewController {
    @IBOutlet weak var container_view: UIView!
    @IBOutlet weak var cancel_btn: UIButton!
    var order_id = 0
    var pageMenu : CAPSPageMenu?
    override func viewDidLoad() {
        super.viewDidLoad()
          setupPageMenu()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let notificationName = Notification.Name("detail_history")
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(detail_historyush),
            name: notificationName,
            object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    func setupPageMenu() {
        var controllerArray : [UIViewController] = []
        let controller_today: HistoryChildTableViewController = HistoryChildTableViewController()
        controller_today.title = "За сегодня"
        controller_today.limit = "today"
        controller_today.parentNavigationController = self.navigationController
        controllerArray.append(controller_today)
        let controller_threeDay: HistoryChildTableViewController = HistoryChildTableViewController()
        controller_threeDay.title = "За 3 дня"
        controller_threeDay.limit = "3days"
        controller_threeDay.parentNavigationController = self.navigationController
        controllerArray.append(controller_threeDay)
        let controller_week: HistoryChildTableViewController = HistoryChildTableViewController()
        controller_week.title = "За неделю"
        controller_week.limit = "week"
        controller_week.parentNavigationController = self.navigationController
        controllerArray.append(controller_week)
        let transparentColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(transparentColor),
            .viewBackgroundColor(transparentColor),
            .selectionIndicatorColor(BaseMessage.textcolor),
            .selectionIndicatorHeight(3.0),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .selectedMenuItemLabelColor(UIColor.white),
            .menuHeight(40.0),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .addBottomMenuHairline(false),
            .centerMenuItems(false)
        ]
        // Initialize page menu with controller array, frame, and optional parameters
        // fix for iphone x , i like the phone(no)
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height != 2436 {
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: container_view.frame.height-40, width: view.frame.width, height: view.frame.height-container_view.frame.height), pageMenuOptions: parameters)
        }
        else {
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: container_view.frame.height, width: view.frame.width, height: view.frame.height-container_view.frame.height), pageMenuOptions: parameters)
        }
        pageMenu!.didMove(toParentViewController: self)
        addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
   // pageMenu!.didMove(toParentViewController: self)
    }
    func willMoveToPage(controller: UIViewController, index: Int){
 
           pageMenu!.moveToPage(index)
    }
    func didMoveToPage(controller: UIViewController, index: Int){
            pageMenu!.moveToPage(index)
    }
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
    @IBAction func cancel_btn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @objc func detail_historyush(notification: NSNotification){
        order_id = notification.object as! Int
        self.performSegue(withIdentifier: "history_cell", sender: nil)
    
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "history_cell" {
            let destination: TravelDetailsController = segue.destination as! TravelDetailsController
            destination.orderid = order_id
            destination.hide_btn =  true
            destination.driver_id = (UserDefaults.standard.integer(forKey: "user_id"))
        }
    }
    // MARK: Configuring UI
}
