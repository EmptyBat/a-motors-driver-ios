//
//  TravelDetailsController.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 11.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD
class TravelDetailsController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var tableView: UITableView!
    var orderid = 0
    var driver_id = 0
    var total_time = 0.0
    var traivelhistory: [TravelDetail] = []
    var data_additional: [TravelDetail] = []
    var casual: [OrderCasual] = []
    var Information: [OrderInformation] = []
    var time_total: [TimeTotal] = []
    var started_at  = ""
    var hide_btn = false
    var force_finish = false
    @IBOutlet weak var cancel_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.dismiss()
        configUI()
        loadData()
        CreateConstraints()
    }
    func loadData() {
        SVProgressHUD.show()
        let url = "\(BaseMessage.baseUrl)/orders/\(orderid)?with[]=driver&with[]=company&with[]=driver.car&with[]=passanger&with[]=driver.car.brand&with[]=driver.car.color"
        print("afafs",url)
        let credentialData = "dev@amotors.kz:123123".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        Alamofire.request(url,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers:headers)
            .validate().responseJSON { response in
            guard response.result.isSuccess else {
                print("Ошибка при запросе данных \(String(describing: response.result.error))")
                return
            }
            SVProgressHUD.dismiss()
            let json = try! JSON(data: (response.data)!)
            for (_,subJson):(String, JSON) in json {
                let new = TravelDetail(json: subJson)
                self.traivelhistory.append(new)
            }
            for (_,subJson):(String, JSON) in json["data"]["data_additional"] {
                let new = TravelDetail(json :subJson)
                self.data_additional.append(new)
            }
            for (_,subJson):(String, JSON) in json["data"]["information"]["casual"] {
                let new = OrderCasual(json :subJson)
                    self.casual.append(new)
            }
            for var i in (0..<json["data"]["information"]["exists"].count)
            {
                i += 1
                let Information = OrderInformation(json :json["data"]["information"]["exists"]["data_additional_\(i)"]["stats"])
                if  json["data"]["information"]["exists"]["data_additional_\(i)"] != nil
                {
                    self.Information.append(Information)
                }
                }
                let Information = OrderInformation(json :json["data"]["information"]["exists"]["data_to"]["stats"])
                self.Information.append(Information)
                
                let total = TimeTotal(json :json["data"]["information"]["stats"])
                self.started_at = json["data"]["information"]["exists"]["data_to"]["time"]["started"].stringValue
                
                self.time_total.append(total)
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
            return 3
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
            if (data_additional.count == 0){
         return 1
            }
            else {
         return 2 + data_additional.count
            }
        }
        else if (section == 1){
        return casual.count
        }
        else {
        return 1
        }
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0) {
        if data_additional.count == 0 {
            return 173
        }
        else {
            if (indexPath.row == 0){
                return 94
            }
            else if (indexPath.row == data_additional.count + 1){
                return 90
            }
            else {
                return 58
            }
            }
        }
        else if (indexPath.section == 1){
            if (indexPath.row == casual.count-1) {
                  return 64
            }
            else {
                  return 39
            }
        }
        else {
            return 138
        }
    }
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 0) {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray
        
        let headerLabel = UILabel(frame: CGRect(x: 28, y: 10, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "Verdana", size: 16)
        headerLabel.textColor = UIColor.white
        headerLabel.text = traivelhistory[0].title
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
        }
        else if (section == 1){
            let headerView = UIView()
            headerView.backgroundColor = UIColor.white
            let headerLabel = UILabel(frame: CGRect(x: 30, y: 0, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height))
            headerLabel.font = UIFont(name: "Verdana", size: 16)
            headerLabel.textColor = UIColor.black
            headerLabel.text = "Дополнительные адреса"
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
            return headerView
        }
        else {
        return nil
        }
    }
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
        return 40
        }
        else if (section == 1){
            if (casual.count != 0){
        return 30
            }
            else {
        return 0 
            }
        }
        else {
        return 0
        }
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0){
            if (data_additional.count != 0){
                if (indexPath.row == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "first_history_cell", for: indexPath) as! HistoryAdditionalFirstTableViewCell
                   // let started_at = traivelhistory[0].started_at
                    
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "HH:mm"
                    
                    let dateFormatteryear = DateFormatter()
                    dateFormatteryear.dateFormat = "dd-MM-yyyy"
                    let current_date = Date()
                    if let date = dateFormatterGet.date(from: started_at){
                        cell.timeStart_lb.text = dateFormatterPrint.string(from: date) + "-"
                    }
                    cell.timeEnd_lb.text = dateFormatterPrint.string(from: current_date)
                    
                    if let date = dateFormatterGet.date(from: started_at){
                        cell.year_lb.text = dateFormatteryear.string(from: date)
                    }
                    
                    cell.districtFrom_lb.text = traivelhistory[0].data_fromName
                    if Information[indexPath.row].time > 60 {
                        cell.distance_lb.text = "\(Information[indexPath.row].distance/1000) км(\(Information[indexPath.row].time / 60) мин)"
                    }
                    else {
                        cell.distance_lb.text = "\(Information[indexPath.row].distance/1000) км(\(Information[indexPath.row].time) сек)"
                    }
                    
                    return cell
                    
                }
                else if (indexPath.row == data_additional.count + 1){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "historylast_cell", for: indexPath) as! HistoryAdditionalLastTableViewCell
                      cell.title_lb.text = traivelhistory[0].data_toName
                    if (time_total[indexPath.section].time_wait > 0){
                          cell.totalStats_lb.text = "Время в пути \(time_total[indexPath.section].time_drive/60) минут,время ожидания \(time_total[indexPath.section].time_wait) минут ,расстояние \(time_total[indexPath.section].distance/1000) км"
                    }
                    else {
                          cell.totalStats_lb.text = "Время в пути \(time_total[indexPath.section].time_drive/60) минут,время ожидания  0 минут ,расстояние \(time_total[indexPath.section].distance/1000) км"
                    }
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "historyadd_cell", for: indexPath) as! HistoryAdditionalTableViewCell
                    print ("afssaf",indexPath.row,"  ",Information.count)
                    cell.title_lb.text = data_additional[indexPath.row-1].name
                    if Information.count-1 >= indexPath.row{
                    if Information[indexPath.row].time > 60 {
                        cell.distance_lb.text = "\(Information[indexPath.row].distance/1000) км(\(Information[indexPath.row].time / 60) мин)"
                    }
                    else {
                        cell.distance_lb.text = "\(Information[indexPath.row].distance/1000) км(\(Information[indexPath.row].time) сек)"
                    }
                    }
                    else {
                        cell.distance_lb.text = ""
                    }
                    return cell
                }
            }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "history_cell", for: indexPath) as! HistoryTableViewCell
                    let started_at = self.started_at
                
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "HH:mm"
                
                    let dateFormatteryear = DateFormatter()
                    dateFormatteryear.dateFormat = "dd-MM-yyyy"
                    let current_date = Date()
                    if let date = dateFormatterGet.date(from: started_at){
                        cell.timeStart_lb.text = dateFormatterPrint.string(from: date) + "-"
                    }
                        cell.timeEnd_lb.text = dateFormatterPrint.string(from: current_date)

                    if let date = dateFormatterGet.date(from: started_at){
                        cell.year_lb.text = dateFormatteryear.string(from: date)
                    }
                if (time_total[indexPath.section].time_wait > 60){
                    cell.totalStats_lb.text = "Время в пути \(time_total[indexPath.section].time_drive/60) минут,время ожидания \(time_total[indexPath.section].time_wait) минут ,расстояние \(time_total[indexPath.section].distance/1000) км"
                }
                else {
                    cell.totalStats_lb.text = "Время в пути \(time_total[indexPath.section].time_drive/60) минут,время ожидания  0 минут ,расстояние \(time_total[indexPath.section].distance/1000) км"
                }
                    cell.districtFrom_lb.text = traivelhistory[0].data_fromName
                    cell.districtTo_lb.text = traivelhistory[0].data_toName
                if (Information.count != 0){
                    if Information[indexPath.row].time > 60 {
                       cell.distance_lb.text = "\(Information[indexPath.row].distance/1000) км(\(Information[indexPath.row].time / 60) мин)"
                    }
                    else {
                    cell.distance_lb.text = "\(Information[indexPath.row].distance/1000) км(\(Information[indexPath.row].time) сек)"
                    }
                }
                else {
                    cell.distance_lb.text = ""
                }
                    return cell
                }
        }
        else if (indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "casual_cell", for: indexPath) as! CasualTableViewCell
             total_time = total_time + Double(casual[indexPath.row].wait)
             cell.district_lb.text = casual[indexPath.row].title
            
            if (Int(casual[indexPath.row].wait)/60 > 1){
             cell.intermediaWait_lb.text = "\(casual[indexPath.row].wait / 60) мин"
            }
            else {
                 cell.intermediaWait_lb.text = "1 минута"
            }
            if (indexPath.row == casual.count - 1) {
                cell.generalWait_lb.text = "Итоговое время ожидания :\(Int(total_time / 60)) минут"
            }
            else {
                cell.generalWait_lb.isHidden = true
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "tax_cell", for: indexPath) as! TaxiInfoTableViewCell
            let car_color = hexStringToUIColor(hex: traivelhistory[0].colorHex)
            cell.brand_lb.text = traivelhistory[0].brand
            cell.color_lb.text = traivelhistory[0].color
            cell.registrationNumber_lb.text = traivelhistory[0].registration_number
            cell.name_lb.text = traivelhistory[0].driverName + " " + traivelhistory[0].driverSurname + " " + traivelhistory[0].driverPatronymic
            cell.color_view.backgroundColor = car_color
            return cell
        }
    }
    func configUI() {
        if (hide_btn == false){
        self.view.addSubview(done_btn)
        }
        if (force_finish == true){
            cancel_btn.isHidden = false
        }
        tableView.register(UINib(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "history_cell")
        tableView.register(UINib(nibName: "HistoryAdditionalFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "first_history_cell")
        tableView.register(UINib(nibName: "HistoryAdditionalTableViewCell", bundle: nil), forCellReuseIdentifier: "historyadd_cell")
        tableView.register(UINib(nibName: "TaxiInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "tax_cell")
        tableView.register(UINib(nibName: "DataAdditionalTableViewCell", bundle: nil), forCellReuseIdentifier: "add_cell")
        tableView.register(UINib(nibName: "DataAdditionaFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "firstadd_cell")
        tableView.register(UINib(nibName: "HistoryAdditionalLastTableViewCell", bundle: nil), forCellReuseIdentifier: "historylast_cell")
        tableView.register(UINib(nibName: "CasualTableViewCell", bundle: nil), forCellReuseIdentifier: "casual_cell")
    }
    
    @IBAction func cancel_btn(_ sender: Any) {
          dismiss(animated: true, completion: nil)
    }
    let done_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("ПОДТВЕРДИТЬ",for: .normal)
        // view.backgroundColor = UIColor(red: 63/255, green: 135/255, blue: 245/255, alpha: 1.0)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(done_btn_clicked), for: .touchUpInside)
        return view
    }()
    func CreateConstraints()  {
        if (hide_btn == false){
        // constraints for forgot button
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height != 2436 {
        done_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        }
        else {
        done_btn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
        }
        done_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        done_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        done_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        }
    }
    @objc func done_btn_clicked(_ sender: AnyObject?) {
        performSegue(withIdentifier: "done", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "done" {
            let destination: CompleteTripViewController = segue.destination as! CompleteTripViewController
               destination.orderid = orderid
               destination.driver_id = driver_id
            
        }
    }


// hex to rgb color
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
}
