//
//  OrderController.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 01.08.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Presentr
import PBRevealViewController
import PusherSwift
import CoreLocation
import SVProgressHUD
class OrderController: UIViewController , UITableViewDelegate,UITableViewDataSource,PusherDelegate,PBRevealViewControllerDelegate,CLLocationManagerDelegate{
    var pusher: Pusher! = nil
    var channel : PusherChannel! = nil
    var status_id : String = ""
    @IBOutlet weak var menu_btn: UIButton!
    @IBOutlet weak var status_lb: UILabel!
    @IBOutlet weak var status_sw: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    var  channel_update_location : PusherChannel! = nil
    var driver_status_t = Timer()
    var location_timer = Timer()
   var locationManager = CLLocationManager()
    var OrdersPullData: [OrderPull] = []
    var OrdersPullFilter: [OrderPull] = []
    let  userid = Int(UserDefaults.standard.string(forKey: "user_id")!)
    override func viewDidLoad() {
        super.viewDidLoad()
        getAcces()
        configUI()
        let frequency_without_order = UserDefaults.standard.integer(forKey: "frequency_without_order")
        self.location_timer = Timer.scheduledTimer(timeInterval:TimeInterval(frequency_without_order/1000), target: self, selector: (#selector(sendLocationToServer)), userInfo: nil, repeats: true)
    }
    @objc func sendLocationToServer(timer: Timer){
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "status_id":status_id,
            "driver_id":(UserDefaults.standard.string(forKey: "user_id")),
            "location": savedData,
            "car_number":(UserDefaults.standard.string(forKey: "car_id")),
            ]
        print("without_order_working",jsonObject)
        channel_update_location.trigger(eventName: "client-location-updated", data:jsonObject)
    }
    func getAcces () {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    func configUI() {
        status_sw.setOn(UserDefaults.standard.bool(forKey: "status_sw"), animated: false)
        var value = 2
        if UserDefaults.standard.bool(forKey: "status_sw") == true{
            status_lb.text = "Готов"
            status_id = "2"
            value = 2
        }
        else {
            status_lb.text = "Не доступен"
               status_id = "3"
               value = 3
        }
        if UserDefaults.standard.bool(forKey: "driverIsBusy") == true {
            status_id = "4"
        }
        status_sw.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.register(UINib(nibName: "OrderPullTableViewCell", bundle: nil), forCellReuseIdentifier: "orderPull_cell")
                tableView.register(UINib(nibName: "OrderPullReserveTableViewCell", bundle: nil), forCellReuseIdentifier: "orderReserve_cell")
        locationManager.startUpdatingLocation()
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        NetworkManager.makeRequest(target: .changeDriverStatus(status_id: value, user_id: userid!,longitude:myLongitude, latitude :myLatitude )){ [weak self]
            (json) in
            guard self != nil else { return }
        }
            self.driver_status_t = Timer.scheduledTimer(timeInterval: 60, target: self, selector: (#selector(changeDriverStatus)), userInfo: nil, repeats: true)
    }
    func createPusher() {
        let pusherClientOptions = PusherClientOptions(authMethod: .inline(secret: "f288a464d3766c8a7fd6"),host: .cluster("ap2"))
        pusher = Pusher(key: "7ea5886145e8af112d5f", options: pusherClientOptions)
        pusher.delegate = self
        let channel_local = pusher.subscribe(channelName: "api-orders-pull")
        let _ = channel_local.bind(eventName: "api-orders-pull.created", callback: { (data: Any?) -> Void in if let data = data as? [String: AnyObject]  {
            let json = JSON(data)
            let new = OrderPull(json: json["order"])
            self.newOrder(new: new)}  })
        
        let _ = channel_local.bind(eventName: "api-orders-pull.updated", callback: { (data: Any?) -> Void in if let data = data as? [String: AnyObject]  {
            let json = JSON(data)
            let new = OrderPull(json: json["order"])
            self.updatedOrder(new: new,order_id :new.order_id)}  })
        
        let _ = channel_local.bind(eventName: "api-orders-pull.accepted", callback: { (data: Any?) -> Void in if let data = data as? [String: AnyObject]  {
            let json = JSON(data)
            let new = OrderPull(json: json["order"])
            self.removeOrder(new: new,order_id :new.order_id)}  })
        
        let _ = channel_local.bind(eventName: "api-orders-pull.declied", callback: { (data: Any?) -> Void in if let data = data as? [String: AnyObject]  {
            let json = JSON(data)
            let new = OrderPull(json: json["order"])
            self.removeOrder(new: new,order_id :new.order_id)}  })
        
        let _ = channel_local.bind(eventName: "api-orders-pull.canceled", callback: { (data: Any?) -> Void in if let data = data as? [String: AnyObject]  {
            let json = JSON(data)
            let new = OrderPull(json: json["order"])
            self.removeOrder(new: new,order_id :new.order_id)}  })
            channel_update_location = pusher.subscribe(channelName: "private-driver-location")
            pusher.connect()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createPusher()
        loadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (pusher != nil){
        pusher.disconnect()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        /*let channel_local = pusher.subscribe(channelName: "private-driver-location")
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let savedData: NSDictionary = ["longitude": "\(myLongitude)",
            "latitude": "\(myLatitude)"]
        let jsonObject:  NSDictionary  = [
            "status_id":status_id,
            "driver_id":(UserDefaults.standard.string(forKey: "user_id")),
            "location": savedData,
            "car_id":(UserDefaults.standard.string(forKey: "car_id")),
            ]
        driver_status_t.invalidate()
        channel_local.trigger(eventName: "client-location-updated", data:jsonObject)*/
    }
    func updatedOrder(new : OrderPull,order_id : String){
        loadData()
        self.tableView.reloadData()
    }
    func newOrder(new : OrderPull){
        self.OrdersPullData.insert(new, at: 0)
        loadData()
        self.tableView.reloadData()
    }
    func removeOrder(new : OrderPull,order_id : String){
        loadData()
        self.OrdersPullFilter = OrdersPullData
        self.tableView.reloadData()
    }
    func loadData() {
        SVProgressHUD.show()
        NetworkManager.makeRequest(target: .ordersPull(city_id: 1,driver_id : UserDefaults.standard.string(forKey: "user_id")!)) { [weak self] (json) in
            guard let vc = self else { return }
            vc.OrdersPullData.removeAll()
            for (index,subJson):(String, JSON) in json["data"] {
                let new = OrderPull(json: subJson)
                if (new.preliminary == true){
                    if (index != "0"){
                  vc.OrdersPullData.append(vc.OrdersPullData[0])
                  vc.OrdersPullData[0] = new
                    }
                    else {
                        vc.OrdersPullData.append(new)
                    }
                }
                else {
                   vc.OrdersPullData.append(new)
                }
            }
            vc.tableView.delegate = self
            vc.tableView.dataSource = self
            vc.tableView.reloadData()
            SVProgressHUD.dismiss()
        }
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table view data source
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray
        
        let headerLabel = UILabel(frame: CGRect(x: 30, y: 10, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "Verdana", size: 20)
        headerLabel.textColor = UIColor.white
    //    headerLabel.text = OrdersPullData[section].title
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
      func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return OrdersPullData.count
    }
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

            return 137
    }
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (OrdersPullData[indexPath.row].preliminary == true){
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderReserve_cell", for: indexPath) as! OrderPullReserveTableViewCell
            let started_at = OrdersPullData[indexPath.row].order_at
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm"
            if let date = dateFormatterGet.date(from: started_at){
                cell.time_lb.text = dateFormatterPrint.string(from: date)
            }
            cell.name_lb.text = OrdersPullData[indexPath.row].passanger_name
            cell.district_lb.text = OrdersPullData[indexPath.row].data_fromName
            cell.comment_lb.text = OrdersPullData[indexPath.row].comment
            if (OrdersPullData[indexPath.row].comment == "null"){
                cell.comment_lb.text = ""
            }
              return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderPull_cell", for: indexPath) as! OrderPullTableViewCell
            let started_at = OrdersPullData[indexPath.row].order_at
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm"
            if let date = dateFormatterGet.date(from: started_at){
                cell.time_lb.text = dateFormatterPrint.string(from: date) 
            }
            cell.name_lb.text = OrdersPullData[indexPath.row].passanger_name
            cell.district_lb.text = OrdersPullData[indexPath.row].data_fromName
            cell.comment_lb.text = OrdersPullData[indexPath.row].comment
            if (OrdersPullData[indexPath.row].comment == "null"){
             cell.comment_lb.text = ""
            }
            return cell
        }
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (self.OrdersPullData[indexPath.row].preliminary == true){
            SVProgressHUD.showError(withStatus: BaseMessage.preliminary_order)
        }
        else{
        pusher.disconnect()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let presenter = Presentr(presentationType: .fullScreen)
        let vc = storyboard.instantiateViewController(withIdentifier: "rootNav")
        customPresentViewController(presenter, viewController: vc, animated: true, completion:  { () in let notificationName = Notification.Name("neworder")
            let order_id = self.OrdersPullData[indexPath.row].order_id
            let passanger_name = self.OrdersPullData[indexPath.row].passanger_name
            let company_name = self.OrdersPullData[indexPath.row].title
            let data :[AnyHashable: Any] = ["passanger_name": passanger_name, "id": order_id,"company": company_name]
            NotificationCenter.default.post(name: notificationName, object: data)
            self.driver_status_t.invalidate()
            self.location_timer.invalidate()
            self.pusher.disconnect()
            })
    }
          self.tableView.deselectRow(at: indexPath, animated: true)
    }
    @objc func switchValueDidChange(_ sender: UISwitch) {
        var getStatus_sw = 0
        locationManager.startUpdatingLocation()
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        if status_sw.isOn == true {
            getStatus_sw = 2
            status_id = "2"
            UserDefaults.standard.set(true, forKey: "status_sw")
            status_lb.text = "Готов"
            let channel_local = pusher.subscribe(channelName: "private-driver-location")
            let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
            let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
            let savedData: NSDictionary = ["longitude": "\(myLongitude)",
                "latitude": "\(myLatitude)"]
            let jsonObject:  NSDictionary  = [
                "status_id":status_id,
                "driver_id":(UserDefaults.standard.string(forKey: "user_id")),
                "location": savedData,
                "car_id":(UserDefaults.standard.string(forKey: "car_id")),
                ]
            print("asfasf",jsonObject)
            channel_local.trigger(eventName: "client-location-updated", data:jsonObject)
        }
        else {
            UserDefaults.standard.set(false, forKey: "status_sw")
            getStatus_sw = 3
            status_id = "3"
            status_lb.text = "Не доступен"
            let channel_local = pusher.subscribe(channelName: "private-driver-location")
            let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
            let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
            let savedData: NSDictionary = ["longitude": "\(myLongitude)",
                "latitude": "\(myLatitude)"]
            let jsonObject:  NSDictionary  = [
                "status_id":status_id,
                "driver_id":(UserDefaults.standard.string(forKey: "user_id")),
                "location": savedData,
                "car_id":(UserDefaults.standard.string(forKey: "car_id")),
                ]
            channel_local.trigger(eventName: "client-location-updated", data:jsonObject)
            }
        NetworkManager.makeRequest(target: .changeDriverStatus(status_id: getStatus_sw, user_id: userid!,longitude:myLongitude, latitude :myLatitude )){ [weak self]
            (json) in
            guard self != nil else { return }
        }
    }
    @objc func changeDriverStatus(timer: Timer) {
        let myLatitude: String = String(format: "%f", locationManager.location!.coordinate.latitude)
        let myLongitude: String = String(format:"%f", locationManager.location!.coordinate.longitude)
        let status_local:Int? = Int(status_id)
        NetworkManager.makeRequest(target: .changeDriverStatus(status_id: status_local ?? 2, user_id: userid!,longitude:myLongitude, latitude :myLatitude )){ [weak self]
            (json) in
            guard self != nil else { return }
        }
    
}
    
    @IBAction func menu_btn(_ sender: Any) {
    revealViewController()?.revealLeftView()
    }
}
