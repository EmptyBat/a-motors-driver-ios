//
//  ShowComplete.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 10.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
class ShowComplete: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addSubObjects()
        CreateConstraints()
        configureUI()
    }
    func configureUI() {
        //  phone_tf.becomeFirstResponder()
    }
    func addSubObjects() {
        view.addSubview(header_img)
        view.addSubview(label_text)
        view.addSubview(cancel_btn)
    }
    let header_img: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "header_main")!
        view.image = image
        view.contentMode = .scaleToFill
        view.clipsToBounds = true
        return view
    }()
    let label_text: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Код принят."
        view.textAlignment = .center
        return view
    }()
    let cancel_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "cancel_icon")!
        view.setImage(image, for: .normal)
        view.addTarget(self, action: #selector(dismissToRootController), for: .touchUpInside)
        return view
    }()
    func CreateConstraints()  {
        // constraints for header image
        header_img.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        header_img.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        header_img.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        header_img.heightAnchor.constraint(equalToConstant: 290).isActive = true
        header_img.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant:-self.view.frame.height - 290).isActive = true
        header_img.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        // constraints for label text
        label_text.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        label_text.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        label_text.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 80).isActive = true
        label_text.heightAnchor.constraint(equalToConstant: 70).isActive = true
        // constraints for cancel button
        cancel_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        cancel_btn.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        cancel_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        cancel_btn.widthAnchor.constraint(equalToConstant: 60).isActive = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
