//
//  CompleteTripViewController.swift
//  A-Motors
//
//  Created by Almas Abdrasilov on 09.07.2018.
//  Copyright © 2018 Almas Abdrasilov. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import InputMask
import SVProgressHUD

class CompleteTripViewController: UIViewController ,UITextFieldDelegate{
    var orderid = 0
    var driver_id = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubObjects()
        CreateConstraints()
        configureUI()        
    }
    let label_text: UILabel = {
        var view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Для завершения поездки введите код, полученный от клиента"
        view.textAlignment = .center
        view.numberOfLines = 0
        view.font = view.font.withSize(17)
        view.textColor = UIColor.black
        return view
    }()
    func configureUI() {
        self.hideKeyboardWhenTappedAround()
        phone_tf.delegate = self
        phone_tf.becomeFirstResponder()
    }
    func addSubObjects() {
        view.addSubview(header_img)
        view.addSubview(label_text)
        view.addSubview(phone_tf)
        view.addSubview(accept_btn)
        view.addSubview(cancel_btn)
    }
    let header_img: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "header_main")!
        view.image = image
        view.contentMode = .scaleToFill
        view.clipsToBounds = true
        return view
    }()
    let phone_tf: UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = NSAttributedString(string: "Код",
                                                        attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        view.textColor = UIColor.black
        view.textAlignment = .center
    
   //     view.borderColor = UIColor.black
        view.layer.borderWidth = 0.2
        view.borderStyle = UITextBorderStyle.bezel
        view.keyboardType = UIKeyboardType.phonePad
        view.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        return view
    }()
    let accept_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("ПОДТВЕРДИТЬ",for: .normal)
        // view.backgroundColor = UIColor(red: 63/255, green: 135/255, blue: 245/255, alpha: 1.0)
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(complete_btn_clicked), for: .touchUpInside)
        return view
    }()
    let cancel_btn: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        var image: UIImage = UIImage(named: "cancel_icon")!
        view.setImage(image, for: .normal)
        view.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
        return view
    }()
    func textFieldDidBeginEditing(_ textField: UITextField) {
        phone_tf.attributedPlaceholder = NSAttributedString(string: "Код",
                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
    }

    @objc func textFieldDidChange(textField: UITextField){
        if (textField.text!.count == 4){
            NetworkManager.makeRequest(target: .finishOrder(order_id: orderid,driver_id :driver_id,confirmation_code :Int((phone_tf.text?.replacingOccurrences(of: " ", with: ""))!)!)){ [weak self] (json) in
                guard let vc = self else { return }
                vc.view.endEditing(true)
                UserDefaults.standard.set(false, forKey: "driverIsBusy")
                UserDefaults.standard.set(false, forKey: "arrived")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc_1 = storyboard.instantiateViewController(withIdentifier: "ordernavig")
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window!.rootViewController = vc_1
                UserDefaults.standard.set(false, forKey: "additional_bool")
                let force_finish = Notification.Name("force_finish_add")
                NotificationCenter.default.post(name: force_finish, object: nil)
                
            }
        }
        print("Text changed")
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 4 // Bool
    }
    func CreateConstraints()  {
        // constraints for header image
        header_img.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        header_img.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        header_img.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        header_img.heightAnchor.constraint(equalToConstant: 290).isActive = true
        header_img.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant:-self.view.frame.height - 290).isActive = true
        header_img.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        // constraints for label text
        label_text.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        label_text.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        label_text.topAnchor.constraint(equalTo: header_img.bottomAnchor, constant: 20).isActive = true
        label_text.heightAnchor.constraint(equalToConstant: 70).isActive = true
        // constraints for phone textfield
        phone_tf.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        phone_tf.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -40).isActive = true
        phone_tf.topAnchor.constraint(equalTo: label_text.bottomAnchor, constant: 20).isActive = true
        phone_tf.heightAnchor.constraint(equalToConstant: 50).isActive = true
        // constraints for forgot button
        accept_btn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        accept_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        accept_btn.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -110).isActive = true
        accept_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        // constraints for cancel button
        cancel_btn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        cancel_btn.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        cancel_btn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        cancel_btn.widthAnchor.constraint(equalToConstant: 60).isActive = true
    }
    @objc func complete_btn_clicked(_ sender: AnyObject?) {
       guard let phone = phone_tf.text, !phone.isEmpty  else {
            SVProgressHUD.showError(withStatus: BaseMessage.fillError)
            return
            }
        NetworkManager.makeRequest(target: .finishOrder(order_id: orderid,driver_id :driver_id,confirmation_code :Int((phone_tf.text?.replacingOccurrences(of: " ", with: ""))!)!)){ [weak self] (json) in
            guard let vc = self else { return }
            vc.view.endEditing(true)
            UserDefaults.standard.set(false, forKey: "driverIsBusy")
            UserDefaults.standard.set(false, forKey: "arrived")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc_1 = storyboard.instantiateViewController(withIdentifier: "ordernavig")
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = vc_1
            SVProgressHUD.showSuccess(withStatus: BaseMessage.orderSucces)
            UserDefaults.standard.set(false, forKey: "additional_bool")
            let force_finish = Notification.Name("force_finish_add")
            NotificationCenter.default.post(name: force_finish, object: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}
}
